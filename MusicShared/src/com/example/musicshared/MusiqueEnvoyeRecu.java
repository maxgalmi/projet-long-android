package com.example.musicshared;

import java.io.Serializable;
import java.util.Date;

public class MusiqueEnvoyeRecu implements Serializable{
	Musique m;
	String pseudo;
	boolean recu; //true si recu, false si envoy�;
	Date date;
	
	public MusiqueEnvoyeRecu(Musique m, String p, boolean b){
		this.m = m;
		pseudo = p;
		this.recu = b; 
		date = new Date();
	}
	
	public Musique getMusique(){
		return m;
	}
	
	public String getPseudo(){
		return pseudo;
	}
	
	public boolean getRecu(){
		return recu;
	}
	
	public Date getDate(){
		return date;
	}
}
