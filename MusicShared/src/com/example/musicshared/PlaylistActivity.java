package com.example.musicshared;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

public class PlaylistActivity extends Activity {
	MusiqueDAO bdd;
	PlaylistAdapter adapter;
	List<Musique> musiquesbdd;
	private File currentDir;
	ListView playlist= null;
	ProgressDialog progress;
	List<Musique>dir;
	 GlobalVariables globalVariables;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_playlist);
		globalVariables = (GlobalVariables) getApplicationContext();
		currentDir = new File(Environment.getExternalStorageDirectory().getPath());
		playlist = (ListView) findViewById(R.id.playlist_listview);
        bdd = new MusiqueDAO(this);
        bdd.open();
        musiquesbdd = bdd.getAllMusiques();
   /*     for (int i = 0; i< musiquesbdd.size();i++)
        	Toast.makeText(getApplicationContext(), musiquesbdd.get(i).getTitre(),Toast.LENGTH_LONG).show();*/
       adapter = new PlaylistAdapter(PlaylistActivity.this,R.layout.file_view2, musiquesbdd);
       playlist.setAdapter(adapter);
       
       playlist.setOnItemClickListener(new OnItemClickListener()
       {
    	   

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				
				final Musique m = adapter.getItem(position);
				PopupMenu popup = new PopupMenu(PlaylistActivity.this, view);  
				popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
				Menu menu =popup.getMenu();
				if (m.getEnvoi())
					menu.add("Interdire l'envoi");
				else
					menu.add("Autoriser Envoi");
				 popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {  
		             public boolean onMenuItemClick(MenuItem item) { 
		            	 if(item.getTitle().equals(getString(R.string.lire))){
		            		
		            		 Intent i = new Intent(Intent.ACTION_VIEW);
		            		 i.setDataAndType(Uri.fromFile(new File(m.getPath())), "audio/mp3");
		            		 startActivity(i);
		            	 }else if(item.getTitle().equals(getString(R.string.details))){
		            		
		            		 Intent i = new Intent(PlaylistActivity.this, DetailMusiqueActivity.class);
		              	   	i.putExtra("titre", m.getTitre());
		              	   		i.putExtra("artiste", m.getArtiste());
		              	   		i.putExtra("genre", m.getGenre());
		            		 
		            		 startActivity(i);
		            	 }else if(item.getTitle().equals("Interdire l'envoi")){
		            		 m.setEnvoi(false);
		            		 bdd.setEnvoi(m.getMd5(), false);
		            	 }else{
		            		 m.setEnvoi(true);
		            		 bdd.setEnvoi(m.getMd5(), true);
		            	 }
		           //  Toast.makeText(PlaylistActivity.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();  
		              return true;  
		             }  
		            });  
				
				 popup.show();
			}
    	});
	}
	
	
	
	 private void fill(File f)
	    {
		
			 dir = new ArrayList<Musique>();
			 
				
				
				dir = getMp3s(f);
				Collections.sort(dir);
					
				
			
			 
	    }
	    
	    //Retourne une liste de tous les fichiers musicaux contenu dans le repertoire dir et ses sous repertoires
	    List<Musique> getMp3s(File dir){
	    	List<Musique>fls = new ArrayList<Musique>();
	        File[] files = dir.listFiles();
	        for(File file : files){
	            if(file.isDirectory()){
	                fls.addAll(getMp3s(file));
	            }else{
	                //add mp3s to list here
	            	String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1).toLowerCase();
	            	if (ext.equals("mp3")){
	            		Mp3File mp3file = null;
						try {
							mp3file = new Mp3File(file.getAbsolutePath());
						} catch (UnsupportedTagException e1) {
							// TODO Auto-generated catch block
							Log.e("UnsupportedTagException", e1.toString());
						} catch (InvalidDataException e1) {
							// TODO Auto-generated catch block
							Log.e("InvalidDataException", e1.toString());
						} catch (IOException e1) {
							// TODO Auto-generated catch block
							Log.e("IOException", e1.toString());
						}
	            		
	            	    String titre = null;
	            	    String artiste = null;
	            	    String genre = null;
	            	    if(mp3file!=null){
		            	    if (mp3file.hasId3v1Tag()){
		            	    	ID3v1 id3v1Tag = mp3file.getId3v1Tag();
		            	    	titre = id3v1Tag.getTitle();
		            	    	artiste = id3v1Tag.getArtist();
		            	    	genre = id3v1Tag.getGenreDescription();
		            	    }else if (mp3file.hasId3v2Tag()) {
		            	    	ID3v2 id3v2Tag = mp3file.getId3v2Tag();
		            	    	titre = id3v2Tag.getTitle();
		            	    	artiste = id3v2Tag.getArtist();
		            	    	genre = id3v2Tag.getGenreDescription();
		            	    }
	            	    }
	            	
	            	    String md5="";
	              	  FileInputStream fis;
	            	    try {
	            			fis = new FileInputStream(file);
	            			md5 = Util.md5(fis);
	            	    	  fis.close();
	            		} catch (Exception e) {
	            			// TODO Auto-generated catch block
	            			e.printStackTrace();
	            		}
	            	    if(titre!=null&&artiste!=null&&!artiste.isEmpty()&&!titre.isEmpty()){
	            	    Musique m = new Musique(titre,artiste,genre,true,file.getAbsolutePath(),md5);
	            	    Musique musiquebdd = bdd.getMusiqueByMD5(md5);
	            	    //Si la musique n'appartient pas d�j� � la bdd
	            	    if (musiquebdd==null){
	            	    	bdd.ajouter(m);	
	            	    	
	            	    }else{
	            	    	if(musiquebdd.getId()!=-1)
	            	    		m.setId(musiquebdd.getId());
	            	    	if(!musiquebdd.getEnvoi())
	            	    		m.setEnvoi(false);
	            	    } fls.add(m);
	            	}
	            	}
	            }	
	            	
	        }
	        return fls;
	    }
	    
	    
	    
	    
	    class scan extends AsyncTask<String, String, Void>{
	    	boolean scan = true;
	    	
	    	 @Override
	    	 protected void onPreExecute(){
	    		
	    		 globalVariables.setScan(true);
	    		 progress = new ProgressDialog(PlaylistActivity.this);
	    		 progress.setMessage("Scan en cours");
	    		
	    		 progress.show();
	    		 progress.setCancelable(false);
	    	 }
			
		    @Override
			protected Void doInBackground(String... params) {
		    	  
		           fill(currentDir);
		          
		           
				return null;
		   		
			}
		    @Override
			protected void onPostExecute(Void v) {
				
		    	 adapter = new PlaylistAdapter(PlaylistActivity.this,R.layout.file_view2,dir);
					playlist.setAdapter(adapter);
					if(progress.isShowing())
						progress.dismiss();
				
				
				Toast.makeText(getApplicationContext(), "Scan termin�" ,Toast.LENGTH_LONG).show();
				GlobalVariables.startMyTask(new majbdd());
			
		    }
		}
	    
	    class majbdd extends AsyncTask<String, String, Void>{
	    	
			
		    @Override
			protected Void doInBackground(String... params) {
		    	  
		    	   List<Musique> musiquesbdd = bdd.getAllMusiques();
		    	   for (int i = 0; i<musiquesbdd.size();i++){
		    		   if(!musiquesbdd.get(i).appartient(dir))
		    			   bdd.supprimer(musiquesbdd.get(i).md5);
		    	   }
				return null;
		   		
			}
		       
		    @Override
			protected void onPostExecute(Void v) {
				//Toast.makeText(getApplicationContext(), "Maj bdd interne" ,Toast.LENGTH_LONG).show();
				globalVariables.setScan(false);
				globalVariables.get_nouvelles_musiques(dir, musiquesbdd);
				if (globalVariables.getNew().size()>0){
					Intent i = new Intent(PlaylistActivity.this, ListeActivity.class);
					startActivity(i);
				}
		    }
		}
	    
	    class attend_fin_scan extends AsyncTask<String, String, Void>{
	    	
	    	@Override
	    	protected void onPreExecute(){
	    		
	    		progress = new ProgressDialog(PlaylistActivity.this);
	    		Toast.makeText(getApplicationContext(), "Scan d�j� en cours..." ,Toast.LENGTH_LONG).show();
	    		 progress.setMessage("Scan en cours");
	    		
	    		 progress.show();
	    		 progress.setCanceledOnTouchOutside(false);
	    		 progress.setCancelable(false);
	    	}
			@Override
			protected Void doInBackground(String... params) {
				// TODO Auto-generated method stub
				while(globalVariables.getScan()){try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}}
				
				
				return null;
			}
			
			protected void onPostExecute(Void v) {
				dir = bdd.getAllMusiques();
		    	 adapter = new PlaylistAdapter(PlaylistActivity.this,R.layout.file_view2,dir);
					playlist.setAdapter(adapter);
					if(progress.isShowing())
						progress.dismiss();
				
				
				Toast.makeText(getApplicationContext(), "Scan termin�" ,Toast.LENGTH_LONG).show();
				globalVariables.get_nouvelles_musiques(dir, musiquesbdd);
				if (globalVariables.getNew().size()>0){
					Intent i = new Intent(PlaylistActivity.this, ListeActivity.class);
					startActivity(i);
				}
			
		    }
			
			
	    	
	    }
	
	    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.playlist, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}else if (id == R.id.scan){
			if(!globalVariables.getScan())
				GlobalVariables.startMyTask(new scan());
			else GlobalVariables.startMyTask(new attend_fin_scan());
		}else if(id == R.id.tri_titre){
			dir = bdd.getAllMusiques();
			Collections.sort(dir, GlobalVariables.titreOrder);
			adapter = new PlaylistAdapter(PlaylistActivity.this,R.layout.file_view2,dir);
			playlist.setAdapter(adapter);
		}else if(id == R.id.tri_genre){
			dir = bdd.getAllMusiques();
			Collections.sort(dir, GlobalVariables.genreOrder);
			adapter = new PlaylistAdapter(PlaylistActivity.this,R.layout.file_view2,dir);
			playlist.setAdapter(adapter);
		}else if(id == R.id.tri_artiste){
			dir = bdd.getAllMusiques();
			Collections.sort(dir, GlobalVariables.artisteOrder);
			adapter = new PlaylistAdapter(PlaylistActivity.this,R.layout.file_view2,dir);
			playlist.setAdapter(adapter);
		}
		return super.onOptionsItemSelected(item);
	}
}
