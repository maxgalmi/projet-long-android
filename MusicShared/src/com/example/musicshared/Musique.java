package com.example.musicshared;

import java.io.File;
import java.io.Serializable;
import java.util.List;




public class Musique implements Comparable<Musique>,Serializable {

	
	private String path;
	String titre;
	String artiste;
	String genre;
	String md5;
	boolean envoi=true;
	 
	int id = -1;
	
	public Musique(String t, String a , String g, boolean e, String p, String m)
	{
		
		path = p;
		titre = t;
		envoi = e;
		artiste = a;
		genre = g;
		md5 = m;
	}
	
	public Musique(String t, String a , String g, boolean e, String p, String m, int id)
	{
		
		path = p;
		titre = t;
		envoi = e;
		artiste = a;
		genre = g;
		md5 = m;
		this.id = id;
	}
	
	public Musique(int i, String t, String a , String g)
	{
		id = i;
		titre = t;
		artiste = a;
		genre = g;
	
	}
	
	public String getPath()
	{
		return path;
	}
	public String getTitre()
	{
		return titre;
	}
	public String getArtiste()
	{
		return artiste;
	}
	public String getGenre()
	{
		return genre;
	}
	public String getMd5()
	{
		return md5;
	}
	public int getId()
	{
		return id;
	}
	public boolean getEnvoi(){
		return envoi;
	}
	public void setEnvoi(boolean b){
		envoi = b;
	}
	public void setId(int i){
		id = i;
	}
	@Override
	public int compareTo(Musique o) {
		if(this.titre != null&&o.getTitre()!=null)
			return this.titre.toLowerCase().compareTo(o.getTitre().toLowerCase()); 
		else if (this.titre != null)
			return -1;
		else return 1;
	}

	public void setTitre(String titre2) {
		// TODO Auto-generated method stub
		titre = titre2;
	}

	public void setArtiste(String artiste2) {
		// TODO Auto-generated method stub
		artiste = artiste2;
	}
	
	public boolean appartient(List<Musique> dir){
    	for(int i = 0; i<dir.size();i++){
    		if(md5.equals(dir.get(i).md5))
    				return true;
    	}return false;
    }


}
