package com.example.musicshared;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AccueilActivity extends Activity implements View.OnClickListener {
	TextView bonjour;
	TextView suggs;
	Button playlist;
	Button historique;
	AlertDialog.Builder bt;
	BluetoothAdapter myBTadapter;
	GlobalVariables globalVariable;
	Button activationShared;
	Button reduire;
	Button quitterApplication;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accueil);
		globalVariable = (GlobalVariables) getApplicationContext();
		
		try {
			FileInputStream fis = openFileInput("MusiquesER_"+globalVariable.getPseudo());
		
		 BufferedInputStream buf = new BufferedInputStream(fis);
		//Toast.makeText(getApplicationContext(), "Fichier trouv�", Toast.LENGTH_SHORT).show();
		 ObjectInputStream is = new ObjectInputStream(buf);
		  globalVariable.setMusiquesER((List<MusiqueEnvoyeRecu>) is.readObject());
		  is.close();
		  fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("Fichier non trouv� - accueil", e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("IOException",e.toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("ClassNotFoundException",e.toString());
		}
		
		try {
			FileInputStream fis = openFileInput("liste_"+globalVariable.getPseudo());
		
		 BufferedInputStream buf = new BufferedInputStream(fis);
		//Toast.makeText(getApplicationContext(), "Fichier trouv�", Toast.LENGTH_SHORT).show();
		 ObjectInputStream is = new ObjectInputStream(buf);
		  globalVariable.setSuggestions((List<Integer>) is.readObject());
		  is.close();
		  fis.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("Fichier non trouv�", e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e("IOException",e.toString());
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			Log.e("ClassNotFoundException",e.toString());
		}
		bt = new AlertDialog.Builder(this);
		bt.setMessage("Vous devez avoir le bluetooth ainsi que la visibilit� activ� "
				+ "de fa�on permanente pour profiter pleinement de l'application. "
				+ "Allez dans les param�tres Bluetooth et selectionnez Aucun d�lai d'expiration de la visibilit�, ou activer la visibilit� temporairement");
		bt.setPositiveButton ("Activer visibilit� 1h", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
	   		 	discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3600);
	   		 	startActivity(discoverableIntent);
				
			}
			
		});
		bt.setNegativeButton("Param�tres Bluetooth", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intentOpenBluetoothSettings = new Intent();
				intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS); 
				startActivity(intentOpenBluetoothSettings); 
			}});
		
		
		
		bonjour = (TextView) findViewById(R.id.bonjour);
		bonjour.setText("Bonjour " + globalVariable.getPseudo());
		suggs = (TextView) findViewById(R.id.suggs);
   			
   		
		playlist = (Button)findViewById(R.id.playlist);
		playlist.setOnClickListener(this);
		historique  = (Button)findViewById(R.id.historique);
		historique.setOnClickListener(this);
		activationShared=(Button)findViewById(R.id.activation);

		
		activationShared.setText("Partage musique");
		
		
		activationShared.setOnClickListener(this);
		reduire =(Button)findViewById(R.id.reduire);

		reduire.setText("R�duire");
		reduire.setOnClickListener(this);
		quitterApplication  = (Button)findViewById(R.id.closeApplication);
		quitterApplication.setOnClickListener(this);
		myBTadapter = BluetoothAdapter.getDefaultAdapter();
	   
		
	}
	
	@Override
	public void onResume(){
		super.onResume();
		GlobalVariables.startMyTask(new get_suggestions());
		if(myBTadapter.getScanMode()!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){
        	bt.show();
   		 	
        }
	}
	
class get_suggestions extends AsyncTask<String, String, Void>{
		
		int musiquesajoute_musiques;
		int musiquesajoute_possede;
	       @Override
		protected Void doInBackground(String... params) {
	    	
	    	   while(globalVariable.getSuggestions()==null){try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
		   		
	       
	   		return null;
		}
		protected void onPostExecute(Void v) {
			suggs.setText("\n Ids musiques sugg�res : \n");
			for (int i = 0; i< globalVariable.getSuggestions().size();i++){
	   			suggs.append(globalVariable.getSuggestions().get(i) + "   ");
	   			
	   		}
	    }
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.accueil, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.deconnexion) {

			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Se deconnecter?");
			builder.setPositiveButton ("Oui", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					SharedPreferences preferences = getSharedPreferences("prefName", MODE_PRIVATE);
					  SharedPreferences.Editor editor = preferences.edit();
					editor.putBoolean("connexion_auto", false);
					  editor.putString("pseudo", "");
					  editor.putString("mdp", "");
					  editor.commit();

					 stopService(new Intent(AccueilActivity.this, ServiceInvisible.class));
					Intent deconnexion = new Intent(AccueilActivity.this, FirstActivity.class);
					  
			    	  startActivity(deconnexion);
			    	  
				}
				
			});
			builder.setNegativeButton("Non", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}});
			builder.show();
		}else if (id == R.id.quitter){
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Etes vous sur de vouloir quitter?");
			builder.setPositiveButton ("Oui", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					

					stopService(new Intent(AccueilActivity.this, ServiceInvisible.class));
					 finish();
				
					  
				}
				
			});
			builder.setNegativeButton("Non", new DialogInterface.OnClickListener(){

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					
				}});
			builder.show();
		}else if(id ==  R.id.action_settings){
					Intent intent = new Intent(this, ParametreActivity.class);
					startActivity(intent);		
					
		}
		return super.onOptionsItemSelected(item);
	}
	@Override
	 public void onBackPressed() {
		   Intent intent = new Intent(Intent.ACTION_MAIN);
		   intent.addCategory(Intent.CATEGORY_HOME);
		   intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		   startActivity(intent);
		   
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.playlist :
				Intent p = new Intent (AccueilActivity.this, PlaylistActivity.class);
				startActivity(p);
				break;
			case R.id.historique : 
				Intent h = new Intent (AccueilActivity.this, HistoriqueActivity.class);
				startActivity(h);
				break;
			case R.id.activation :
				startService(new Intent(this,ServiceInvisible.class));
				break;
			case R.id.reduire : 
				this.moveTaskToBack(true);
				break;
			case R.id.closeApplication : 
				stopService(new Intent(this,ServiceInvisible.class));
				finish();
				break;
				
		
		}
	
	}
}
