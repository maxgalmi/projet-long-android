package com.example.musicshared;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

import android.util.Log;

public class FonctionsTransfert {
	public static String lireTexte2(InputStream in){
		String s="vide";
				try {
					ObjectInputStream bf = new ObjectInputStream(in);
					 
					s = (String)bf.readObject();
					Log.e("lecture ligne","lecture "+s);

					//bf.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.e("liretexte","erreur lecture "+e.toString());

					e.printStackTrace();
				} 
		
		return s;
	}
	
	
	public static int lireInt(InputStream in){
	
	int ss=0;
				try {
					ObjectInputStream bf = new ObjectInputStream(in);
					 while(in.available()<1){
					Thread.sleep(200);	 
					Log.e("lireint attente",""+in.available());

					 }
					 
					ss = bf.readInt();
					Log.e("lecture int de taille de byte","int taille = "+ss);
					//s=ss.longValue();
					//bf.close();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					Log.e("lireint","erreur lecture2 "+e.toString());

					e.printStackTrace();
				} 
		
		return ss;
	}
	
	public static byte[] lireByte(InputStream in,int taillebyte){				
		byte[] data = new byte[taillebyte];
		try {
			Log.e("lirebyte","available byte lu "+in.available());
			 while(in.available()<taillebyte){
					Thread.sleep(100);	 
					Log.e("lirebyte attente",""+in.available());
					 }
			int count = in.read(data,0,taillebyte);
			Log.e("lirebyte",count+" byte lu "+" texte:"+(new String (data))+" taille:"+data.length);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("lirebyte","erreur lecture "+e.toString());

			e.printStackTrace();
		}
		return data;
	}
	
	public static void receptionFichier(InputStream in,String chemin){
		
		 try {
				int nbbytelu=0;
				int nbbytefile=lireInt(in);

			FileOutputStream out=new FileOutputStream(chemin);
	            byte buf[] = new byte[1024];
	            int n=0;

	            try{
	            while((n=in.read(buf))!=-1){
		        	Log.e("receptionfichier","lecture de nb bit: "+n+"total="+nbbytelu+" totalattendu"+nbbytefile+" available="+in.available());

	            	out.write(buf,0,n);
	            	nbbytelu+=n;

	            }

	            }catch(IOException e){
	            	Log.e("receptionfichier","socketferme");
	            }
	        	Log.e("receptionfichier","fin lecture: "+n+"total="+nbbytelu+" totalattendu"+nbbytefile+" available="+in.available());

	            out.close();                    
	            
	        } catch (Exception ex) {
	        	Log.e("receptionfichier","erreur lecture "+ex.toString());
	        	
	        }
		
	}
		
	public static void ecrireTexte2(OutputStream out,String s) throws Cancel{
		
		try {
			Log.e("ECRIRETEXTE2",s);
		ObjectOutputStream pw = new ObjectOutputStream (out);
				 
					pw.writeObject(s);
					pw.flush(); 
					 
					 //pw.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			        cancel("transmissionfichier");

				} 
			
		
		
		
	}
	

	

	public static void ecrireInt(OutputStream out,int s) throws Cancel{
		
		try {
			Log.e("ECRIRETEXTE2int ","taille int envoyer "+s);
			ObjectOutputStream pw = new ObjectOutputStream (out);


				 
					pw.writeInt(s);
					pw.flush(); 
					 
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			        cancel("transmissionfichier");

				} 
					
	}
	
	
	
	
	
	public static int discussionExpediteur(InputStream in,OutputStream out, List<Musique> list) throws Cancel{
		int message= -1 ;//"plusdeproposition";
		Boolean finMessage=false;
		while(!finMessage){
			message=lireInt(in);
			int id;
			int x = GlobalVariables.appartient(message, list);
			if (x!=-1 && list.get(x).getEnvoi()) id = message;
			else id = -1;
			if(message==-1 || id !=-1 ){
				        ecrireTexte2(out,"oui");
				        finMessage=true;
			}
			else  ecrireTexte2(out,"non");	
		}
		return message;
	}

	public static int discussionRecepteur(InputStream in,OutputStream out, List<Integer> suggestions) throws Cancel{
		int message=-1;
		
		if(suggestions!=null){
		  for(int i=0; i< suggestions.size();i++){
				  message = suggestions.get(i);
				  ecrireInt(out,message);
				  if(lireTexte2(in).equals("oui"))
					 return message;
		  }
		}
			ecrireInt(out,-1);
			message=-1;
		return message;
	}
	
	
	public static void transmissionFichier(OutputStream out,String chemin) throws Cancel{

		Log.e("transmissionficgher",""+tailleFichier(chemin));
		long taille=tailleFichier(chemin);
		int envoyer=0;
		int tailleTabByte=200;
		try {
		FileInputStream inf;
		File f=getFileSave(chemin);
		Log.e("transmissionficgher",""+tailleFichier(chemin));

		if(f.exists()){
			inf = new FileInputStream(f);
		
			 	byte buf[] = new byte[tailleTabByte];
	           
	            int n;                   
	            ecrireInt(out,((int)taille));

	            while((n=inf.read(buf))!=-1){
	            		
	                    out.write(buf,0,n);

	       			out.flush();
	       			envoyer+=n;
	       		Log.e("transmissionfichier","tansmissiin de nb bit:"+n+" tailletotalfile="+taille+" envoyer "+envoyer);

	            }           
	           // ecrireInt(out,0);
	       		Log.e("transmissionfichier","fin tansmissiin de nb bit:"+n+" tailletotalfile="+taille+" envoyer "+envoyer);

	            inf.close();
	             
		}
		else{
	   		Log.e("file n'xiste pas","chemin:"+chemin);

		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("transmissionfichier","erreur "+e.toString());

			e.printStackTrace();
	        cancel("transmissionfichier");

		}
		
	}


	public static long tailleFichier(String s){
		
		File f=new File(s);
		if(f.exists()){
		long taille =0;
		
		taille=f.length();
		return taille;
		}
		else{
			return (long) 0;
		}
	}


	public static File getFileSave(String chemin){
		File f=new File(chemin);
		if(f.exists()){
			Log.e("getfilesave","existe "+f.getAbsolutePath());

		}
		else{
			Log.e("getfilesave","existepas");
		}
return f;
		
	}


public static void cancel(String s) throws Cancel{
	throw new Cancel("lancement exception personalisť pour supprimer socket par la fonction "+s);
	
}









}
