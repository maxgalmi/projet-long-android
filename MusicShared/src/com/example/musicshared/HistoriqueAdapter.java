package com.example.musicshared;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class HistoriqueAdapter extends ArrayAdapter<MusiqueEnvoyeRecu> {
	private Context c;
	private int id;
	private List<MusiqueEnvoyeRecu>items;
	public HistoriqueAdapter(Context context,
			int textViewResourceId, List<MusiqueEnvoyeRecu> objects) {
		super(context, textViewResourceId, objects);
		c = context;
		id = textViewResourceId;
		items = objects;
	}
	

	public MusiqueEnvoyeRecu getItem(int i)
	 {
		 return items.get(i);
	 }
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(id, null);
            }
           // CheckBox cBox = (CheckBox) v.findViewById(R.id.chk);
           
            final MusiqueEnvoyeRecu o = items.get(position);
            if (o != null) {
                TextView t1 = (TextView) v.findViewById(R.id.TextView03);
                TextView t2 = (TextView) v.findViewById(R.id.TextView04);
               
                if(t1!=null)
                	t1.setText(o.getMusique().getTitre() + " - " + o.getMusique().getArtiste());
                if(t2!=null){
                	if(o.getRecu())
                		t2.setText("Re�u de ");
                	else
                		t2.setText("Envoy� � ");
                	t2.append(o.getPseudo() + " le " + o.getDate().toString());
                }
        }
           
           
            return v;
    }
	

}
