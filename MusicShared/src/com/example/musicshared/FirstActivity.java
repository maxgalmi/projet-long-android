package com.example.musicshared;
import java.io.FileInputStream;
import java.util.*;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

@SuppressLint("ClickableViewAccessibility")
public class FirstActivity extends Activity implements OnTouchListener{
	
	private Button loginButton = null;
	private Button inscription = null;
	String pseudo;
	String mdp;
	int id_utilisateur;
	AlertDialog.Builder bt;
	BluetoothAdapter myBTadapter;
	GlobalVariables globalVariables;
	
	private OnClickListener clickListenerLogin = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(FirstActivity.this, ConnexionActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
	    }
	};
	
	private OnClickListener clickListenerInscription= new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(FirstActivity.this, InscriptionActivity.class);
			startActivity(intent);
	    }
	};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		globalVariables = (GlobalVariables) getApplicationContext();
		bt = new AlertDialog.Builder(this);
		bt.setMessage("Vous devez avoir le bluetooth ainsi que la visibilit� activ� "
				+ "de fa�on permanente pour profiter pleinement de l'application. "
				+ "Allez dans les param�tres Bluetooth et selectionnez Aucun d�lai d'expiration de la visibilit�, ou activer la visibilit� temporairement");
		bt.setPositiveButton ("Activer visibilit� 1h", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
	   		 	discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3600);
	   		 	startActivity(discoverableIntent);
				
			}
			
		});
		bt.setNegativeButton("Param�tres Bluetooth", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intentOpenBluetoothSettings = new Intent();
				intentOpenBluetoothSettings.setAction(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS); 
				startActivity(intentOpenBluetoothSettings); 
			}});
		loginButton = (Button)findViewById(R.id.button2);
		inscription = (Button)findViewById(R.id.button1);
		inscription.setOnClickListener(clickListenerInscription);
		
		//offlineButton = (Button)findViewById(R.id)
		loginButton.setOnClickListener(clickListenerLogin); 
		//offlineButton.setOnClickListenr(clickListenerOffline);
		SharedPreferences preferences = getSharedPreferences("prefName", MODE_PRIVATE); 
        Map<String, ?> allEntries = preferences.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
        }
         myBTadapter = BluetoothAdapter.getDefaultAdapter();
        
         //Si la connexion auto a �t� activ�, on passe directement � l'activit� suivante
	   if(preferences.getBoolean("connexion_auto",false)){
		
	    	pseudo = preferences.getString("pseudo", "");
	    	mdp = preferences.getString("mdp","");
	    	id_utilisateur = preferences.getInt("id_utilisateur", 0);	
	    	globalVariables.setPseudo(pseudo);
	    	globalVariables.setId(id_utilisateur);
	    	Intent intent = new Intent(FirstActivity.this,ListeActivity.class);
	    	startActivity(intent);
	    }
	}
			
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
		case R.id.action_settings:
			Intent intent = new Intent(FirstActivity.this, ParametreActivity.class);
			startActivity(intent);		
			return true;
		case R.id.action_disconnection:
			finish();
			return true;
		default:	
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onResume(){
		super.onResume();
		if(myBTadapter.getScanMode()!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE){
        	bt.show();
   		 	
        }
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}
