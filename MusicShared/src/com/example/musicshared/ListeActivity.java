package com.example.musicshared;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;




import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;



import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

public class ListeActivity extends Activity implements View.OnClickListener{
	MusiqueDAO bdd;
    private File currentDir;
    private FileArrayAdapter adapter;
    Button valider;
    public static String titre;
    public static  String artiste;
    ListView liste = null;
    ProgressDialog progress;
    List<Musique>dir;
    List<Musique> musiquesbdd;
    boolean premiereutilisation;
    boolean pass1 = false;
    boolean pass2 = false;
    CheckBox cBox; 
    GlobalVariables globalVariables;
    boolean parametres;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.liste);
        globalVariables = (GlobalVariables) getApplicationContext();
        //Toast.makeText(getApplicationContext(), "id : " + globalVariables.getId(),Toast.LENGTH_LONG).show();
        currentDir = new File(Environment.getExternalStorageDirectory().getPath());
        liste = (ListView) findViewById(R.id.listView);
        bdd = new MusiqueDAO(this);
        bdd.open();
      
      
        valider = (Button) findViewById(R.id.button3);
        
        
        valider.setOnClickListener(this);
        
        valider.setEnabled(false);
       
        valider.setOnClickListener(this);
        SharedPreferences preferences = getSharedPreferences("prefName", MODE_PRIVATE); 
        cBox = (CheckBox) findViewById(R.id.chk);
        premiereutilisation = preferences.getBoolean(globalVariables.getPseudo(), true);
        Intent i = getIntent();
        parametres = i.getBooleanExtra("parametres", false);
       // Toast.makeText(getApplicationContext(), "Premire utilisation : " +  premiereutilisation ,Toast.LENGTH_LONG).show();
        if(!premiereutilisation&&globalVariables.getNew()!=null
        		&&globalVariables.getNew().size()!=0){
        	 adapter = new FileArrayAdapter(ListeActivity.this,R.layout.file_view,globalVariables.getNew());
        	 dir = globalVariables.getNew();
        	 liste.setAdapter(adapter);
        	 this.setTitle("Nouvelles musiques trouv�es !");
        	 valider.setEnabled(true);
        	
        }
        else{
        	/*if(globalVariables.getMaj()||globalVariables.getScan()){
        		new attend_fin_scan().execute();
        	}else{*/
        		new scan().execute();
        	//}
        	
        	
        	
        	 if (!premiereutilisation&&!parametres){
             	new majbdd().execute();
             	new ajout_musiques().execute();
             	Intent accueil = new Intent(ListeActivity.this, AccueilActivity.class);
            	accueil.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
     	    	   startActivity(accueil);
             }
        }
       
    }
    private void fill(File f)
    {
    //	File[]dirs = f.listFiles();
		
		 dir = new ArrayList<Musique>();
		 
			
			
			dir = getMp3s(f);
			Collections.sort(dir);
				
			
		
		/* if(!f.getName().equalsIgnoreCase("sdcard"))
			 dir.add(0,new Musique("..","Parent Directory",f.getParent()));*/
		 adapter = new FileArrayAdapter(ListeActivity.this,R.layout.file_view,dir);
		 
    }
    
    //Retourne une liste de tous les fichiers musicaux contenu dans le repertoire dir et ses sous repertoires
    List<Musique> getMp3s(File dir){
    	List<Musique>fls = new ArrayList<Musique>();
        File[] files = dir.listFiles();
        int n = 0;
        for(File file : files){
            if(file.isDirectory()){
                fls.addAll(getMp3s(file));
            }else{
                //add mp3s to list here
            	String ext = file.getName().substring(file.getName().lastIndexOf(".") + 1).toLowerCase();
            	if (ext.equals("mp3")){
            		Mp3File mp3file = null;
					try {
						mp3file = new Mp3File(file.getAbsolutePath());
					} catch (UnsupportedTagException e1) {
						// TODO Auto-generated catch block
						Log.e("UnsupportedTagException", e1.toString());
					} catch (InvalidDataException e1) {
						// TODO Auto-generated catch block
						Log.e("InvalidDataException", e1.toString());
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						Log.e("IOException", e1.toString());
					}
            		
            	    String titre = null;
            	    String artiste = null;
            	    String genre = null;
            	    if(mp3file!=null){
	            	    if (mp3file.hasId3v1Tag()){
	            	    	ID3v1 id3v1Tag = mp3file.getId3v1Tag();
	            	    	titre = id3v1Tag.getTitle();
	            	    	artiste = id3v1Tag.getArtist();
	            	    	genre = id3v1Tag.getGenreDescription();
	            	    }else if (mp3file.hasId3v2Tag()) {
	            	    	ID3v2 id3v2Tag = mp3file.getId3v2Tag();
	            	    	titre = id3v2Tag.getTitle();
	            	    	artiste = id3v2Tag.getArtist();
	            	    	genre = id3v2Tag.getGenreDescription();
	            	    }
            	    }
            	    String md5="";
              	  FileInputStream fis;
            	    try {
            			fis = new FileInputStream(file);
            			md5 = Util.md5(fis);
            	    	  fis.close();
            		} catch (Exception e) {
            			// TODO Auto-generated catch block
            			e.printStackTrace();
            		}
            	    if(titre!=null&&artiste!=null&&!artiste.isEmpty()&&!titre.isEmpty()){
            	    Musique m = new Musique(titre,artiste,genre,true,file.getAbsolutePath(),md5);
            	    Musique musiquebdd = bdd.getMusiqueByMD5(md5);
            	    //Si la musique n'appartient pas d�j� � la bdd
            	    if (musiquebdd==null){
            	    	bdd.ajouter(m);	
            	    	
            	    }else{
            	    	if(musiquebdd.getId()!=-1)
            	    		m.setId(musiquebdd.getId());
            	    	if(!musiquebdd.getEnvoi())
            	    		m.setEnvoi(false);
            	    } fls.add(m);
            	}
            	}
            }	
            	
        }
        return fls;
    }
    
   
    class scan extends AsyncTask<String, String, Void>{
    	
    	@Override
		protected void onPreExecute(){
			globalVariables.setScan(true);
			if(premiereutilisation||parametres){
   		 	progress = new ProgressDialog(ListeActivity.this);
   		 	progress.setMessage("Scan en cours");
   		
   		 	progress.show();
   		 	progress.setCancelable(false);
			}
		}
    	
	    @Override
		protected Void doInBackground(String... params) {
	    	
	    	   
	           fill(currentDir);
	          
	           
			return null;
	   		
		}
	    @Override
		protected void onPostExecute(Void v) {
			
			if(premiereutilisation||parametres){
				liste.setAdapter(adapter);
				if(progress.isShowing())
				progress.dismiss();
			}
			
			Toast.makeText(getApplicationContext(), "Scan termin�" ,Toast.LENGTH_LONG).show();
			
			
			new majbdd().execute();
		
	    }
	}
    

    
    //supprime de la bdd interne les musiques n'�tant plus dans le t�l�phone
    class majbdd extends AsyncTask<String, String, Void>{
    	
		
	       @Override
		protected Void doInBackground(String... params) {
	    	  // while(!pass1){}
	    	   List<Musique> musiquesbdd = bdd.getAllMusiques();
	    	   for (int i = 0; i<musiquesbdd.size();i++){
	    		   if(!musiquesbdd.get(i).appartient(dir))
	    			   bdd.supprimer(musiquesbdd.get(i).md5);
	    	   }
			return null;
	   		
		}
		protected void onPostExecute(Void v) {
			//Toast.makeText(getApplicationContext(), "Maj bdd interne" ,Toast.LENGTH_LONG).show();
			globalVariables.setMaj(true);
			globalVariables.setScan(false);
			
			if(premiereutilisation||parametres){
				
				valider.setEnabled(true);
			}
			pass2 = true;
	    }
	}
    


    
    //Ajoute musiques � la bdd externe
    class ajout_musiques extends AsyncTask<String, String, Void>{
		
		int musiquesajoute_musiques;
		int musiquesajoute_possede;
	       @Override
		protected Void doInBackground(String... params) {
	    	   //while(!pass2){}
	    	
	    	   Socket s;
			try {
				Log.e("Connexion", "connexion au serveur");
				
				s = new Socket("musicshared.ddns.net",6660);
				Log.e("Connexion", "connect� au serveur");
				OutputStream os = s.getOutputStream(); 
		    	ObjectOutputStream oos = new ObjectOutputStream(os); 
		    	//Envoi du pseudo au serveur
		    	Log.e("Connexion", "Envoi du pseudo au serveur");
		    	oos.writeObject(globalVariables.getId());
		    	//envoi de la playlist au serveur
		    	oos.writeObject(dir);
		    	
		    	InputStream is = s.getInputStream();
		    	ObjectInputStream ois = new ObjectInputStream(is);
		    	musiquesajoute_musiques = (Integer) ois.readObject();
		    	musiquesajoute_possede = (Integer) ois.readObject();
		    	//reception de la playlist avec les ids mis � jourv
		    	Log.e("Connexion", "reception de la playlist avec les ids mis � jour");
		    	dir = (List<Musique>) ois.readObject();
		    	List<Integer> sugg = (List<Integer>) ois.readObject();
		    	Log.e("Connexion", "ajout suggestion");
		    	if(sugg.size()>0){
		    	globalVariables.viderSuggestions();
		    	for(int  i=0; i < sugg.size();i++){
		    		if(GlobalVariables.appartient(sugg.get(i), dir)==-1){
		    			globalVariables.addSuggestion(sugg.get(i));
		    		}
		    	}
		    	}
		    	
		    	
		    	oos.close();
		    	os.close(); 
		    	ois.close();
		    	is.close();
		    	 
		    	s.close(); 
		    	if(globalVariables.getSuggestions()!=null){
		    	FileOutputStream fos = openFileOutput("liste_" + globalVariables.getPseudo(), Context.MODE_PRIVATE);
				ObjectOutputStream os2 = new ObjectOutputStream(fos);
				 os2.writeObject(globalVariables.getSuggestions());
				 os2.close();
				 fos.close();
		    	}
		    	//maj des ids de la bdd interne
		    	for (int i = 0;i<dir.size();i++){
		    		bdd.setId(dir.get(i).getMd5(), dir.get(i).getId());
		    	}
		    	
		    	
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e("Fail",e.toString());
			}
	    	   
	    	
	       
	   		return null;
		}
		protected void onPostExecute(Void v) {
			globalVariables.setMaj(false);
			//Toast.makeText(getApplicationContext(), "Musiques ajout�s (musiques) : " + musiquesajoute_musiques,Toast.LENGTH_LONG).show();
			//Toast.makeText(getApplicationContext(), "Musiques ajout�s (possede) : " + musiquesajoute_possede,Toast.LENGTH_LONG).show();
	    }
	}
    
    public List<Musique> get_musiques(){
    	musiquesbdd = new ArrayList<Musique>();
    	InputStream is = null;
		String result = "";
		String line;
 	   
   		// Envoie de la commande http
    	try
    	{
		HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost("http://musicshared.netne.net/get_musiques.php");
	        HttpResponse response = httpclient.execute(httppost); 
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
	        Log.e("pass 1", "connection success ");
    	}
        catch(Exception e)
    	{
        	Log.e("Fail 1", e.toString());
    	}    

   		// Conversion de la requ�te en string
        try
        {
            BufferedReader reader = new BufferedReader
			(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
	    {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
	    Log.e("pass 2", "connection success ");
        }
        catch(Exception e)
        {
            Log.e("Fail 2", e.toString());
        } 
    	try
    	{	JSONArray jArray  = new JSONArray(result);
    		
    		for(int i=0;i<jArray.length();i++){
    			JSONObject json_data = jArray.getJSONObject(i);
    			
    			musiquesbdd.add(new Musique(Integer.parseInt(json_data.getString("id_musique")),json_data.getString("titre"),
    					json_data.getString("artiste"),json_data.getString("genre")));
    			Log.e("Pass 3", musiquesbdd.get(i).getTitre() + " trouv�");
			 }
    		
    	}
        catch(Exception e)
    	{  
        	Log.e("Fail 3 - connexion", e.toString());
    	}return musiquesbdd;
    }
    
    public int add_musiques(List<Musique> m){
 	   InputStream is = null;
  		String result="";
  		int musiquesajoute=0;
  		
  		String json_string ="{\"musiques\":[";
  		
  		try {
  			for(int i = 0;i<m.size();i++){
  			JSONObject obj_new = new JSONObject();
			obj_new.put("titre", m.get(i).getTitre());
			obj_new.put("artiste", m.get(i).getArtiste());
			obj_new.put("genre", m.get(i).getGenre());
			json_string = json_string + obj_new.toString() + ",";
  			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
  	//Close JSON string
  		json_string = json_string.substring(0, json_string.length()-1);
  		json_string += "]}";
  		Log.e("json - add_musiques",json_string);

  		// Envoie de la commande http
  		try{
  			HttpClient httpclient = new DefaultHttpClient();
  			HttpPost httppost = new HttpPost("http://musicshared.netne.net/ajout_musique.php");
  			httppost.setEntity(new ByteArrayEntity(json_string.getBytes("UTF8")));
  			httppost.setHeader("json", json_string);
  			HttpResponse response = httpclient.execute(httppost);
  			HttpEntity entity = response.getEntity();
  			is = entity.getContent();
  			Log.e("pass 1 - insert", "connection success ");
  		
  		}catch(Exception e){
  			Log.e("Fail 1", e.toString());
  		}
  		try{
  			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
  			StringBuilder sb = new StringBuilder();
  			String line = null;
  			while ((line = reader.readLine()) != null) {
  				sb.append(line + "\n");
  			}
  			is.close();
  			result=sb.toString();
  			Log.e("pass 2", "connection success ");
  		}catch(Exception e){
  			Log.e("Fail 2", e.toString());
  		}
  		
  		
  		try
    	{
        	JSONObject json_data = new JSONObject(result);
        	musiquesajoute=(json_data.getInt("code"));
        
    	}
        catch(Exception e)
    	{   
        	Log.e("Fail 3 - connexion", e.toString());
    	}
    	return musiquesajoute;
    }
    
    public int add_possede(List<Musique> m){
  	   InputStream is = null;
   		String result="";
   		int musiquesajoute=0;
   		
   		String json_string ="{\"possede\":[";
   		
   		try {
   			for(int i = 0;i<m.size();i++){
   			JSONObject obj_new = new JSONObject();
   		
 			obj_new.put("id_utilisateur", globalVariables.getId());
 			obj_new.put("titre", m.get(i).getTitre());
 			obj_new.put("artiste", m.get(i).getArtiste());
 			json_string = json_string + obj_new.toString() + ",";
   			}
 		} catch (JSONException e1) {
 			// TODO Auto-generated catch block
 			e1.printStackTrace();
 		}
   	//Close JSON string
   		json_string = json_string.substring(0, json_string.length()-1);
   		json_string += "]}";
   		Log.e("json",json_string);
   		// Envoie de la commande http
   		try{
   			HttpClient httpclient = new DefaultHttpClient();
   			HttpPost httppost = new HttpPost("http://musicshared.netne.net/ajout_possede.php");
   			httppost.setEntity(new ByteArrayEntity(json_string.getBytes("UTF8")));
   			httppost.setHeader("json", json_string);
   			HttpResponse response = httpclient.execute(httppost);
   			HttpEntity entity = response.getEntity();
   			is = entity.getContent();
   			Log.e("pass 1 - possede", "connection success ");
 
   		}catch(Exception e){
   			Log.e("Fail 1", e.toString());
   		}
   		try{
   			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
   			StringBuilder sb = new StringBuilder();
   			String line = null;
   			while ((line = reader.readLine()) != null) {
   				sb.append(line + "\n");
   			}
   			is.close();
   			result=sb.toString();
   			Log.e("pass 2", "connection success ");
   		}catch(Exception e){
   			Log.e("Fail 2", e.toString());
   		}
   		
   		
   		try
     	{
         	JSONObject json_data = new JSONObject(result);
         	musiquesajoute=(json_data.getInt("code"));
         
     	}
         catch(Exception e)
     	{   
         	Log.e("Fail 3 - possede", e.toString());
     	}
     	return musiquesajoute;
     }
    
    class attend_fin_scan extends AsyncTask<String, String, Void>{
    	
    	@Override
    	protected void onPreExecute(){
    		
    		progress = new ProgressDialog(ListeActivity.this);
    		Toast.makeText(getApplicationContext(), "Scan d�j� en cours..." ,Toast.LENGTH_LONG).show();
    		 progress.setMessage("Scan en cours");
    		
    		 progress.show();
    		 progress.setCanceledOnTouchOutside(false);
    		 progress.setCancelable(false);
    	}
		@Override
		protected Void doInBackground(String... params) {
			// TODO Auto-generated method stub
			while(globalVariables.getScan()||globalVariables.getMaj()){try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
			
			
			return null;
		}
		
		protected void onPostExecute(Void v) {
			dir = bdd.getAllMusiques();
	    	 adapter = new FileArrayAdapter(ListeActivity.this,R.layout.file_view,dir);
				liste.setAdapter(adapter);
				if(progress.isShowing())
					progress.dismiss();
			
			valider.setEnabled(true);
			Toast.makeText(getApplicationContext(), "Scan termin�" ,Toast.LENGTH_LONG).show();
		//	globalVariables.get_nouvelles_musiques(dir, musiquesbdd);
			
		
	    }
		
		
    	
    }
    
   
@Override
	public void onClick(View v) {
	// TODO Auto-generated method stub
		switch(v.getId()){
			case R.id.button3:
				for (int i = 0; i <adapter.checked.length; i++){	
						bdd.setEnvoi(dir.get(i).getMd5(), adapter.checked[i]);	
				}
				new ajout_musiques().execute();
				
				SharedPreferences preferences = getSharedPreferences("prefName", MODE_PRIVATE);
				  SharedPreferences.Editor editor = preferences.edit();
	
				  editor.putBoolean(globalVariables.getPseudo(), false);
				 
				  editor.commit();
				  globalVariables.viderNew();
				Intent accueil = new Intent(ListeActivity.this, AccueilActivity.class);
				accueil.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		    	startActivity(accueil);
		    	
		    	 
		    	   break;
			
				
				
				
		}
	
	}
}
