package com.example.musicshared;



import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class MusiqueDAO {
	private SQLiteDatabase bdd;
	private BaseSQLite baseSQLite;
	private static final int VERSION_BDD = 1;
	private static final String NOM_BDD = "playlist.db";
	  public static final String TABLE_NAME = "musiques";

	  public static final String KEY = "id";

	  public static final String TITRE = "titre";
	  private static final int NUM_COL_TITRE = 0;
	  public static final String ARTISTE = "artiste";
	  private static final int NUM_COL_ARTISTE = 1;
	  
	  private static final int NUM_COL_GENRE = 2;
	  public static final String GENRE = "genre";
	  private static final int NUM_COL_ENVOI = 3;
	  public static final String ENVOI = "envoi";
	  private static final int NUM_COL_PATH = 4;
	  public static final String PATH = "path";
	  private static final int NUM_COL_MD5 = 5;
	  public static final String MD5 = "md5";
	  private static final int NUM_COL_KEY = 6;
	  public static final String TABLE_CREATE =
			    "CREATE TABLE " + TABLE_NAME + " (" +
			     MD5 + " TEXT PRIMARY KEY , " +
			      TITRE + " TEXT NOT NULL, " +
			      ARTISTE + " TEXT NOT NULL, " +
			      GENRE + " TEXT, " +
			      ENVOI + " INTEGER NOT NULL, " +
			      KEY + " INTEGER);";

	  public static final String TABLE_DROP =  "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

	 
	  public MusiqueDAO(Context c){
		  baseSQLite = new BaseSQLite(c, NOM_BDD, null, VERSION_BDD);
	  }
	  
	  public void open(){
			//on ouvre la BDD en �criture
			bdd = baseSQLite.getWritableDatabase();
	}
	  
	  public void close(){
			//on ferme l'acc�s � la BDD
			bdd.close();
		}
	  
	  /**

	   * @param m la musique � ajouter � la base
	 * @return 

	   */
	  public long ajouter(Musique m) {
		//Cr�ation d'un ContentValues (fonctionne comme une HashMap)
			ContentValues values = new ContentValues();
			//on lui ajoute une valeur associ� � une cl� (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
			values.put(TITRE, m.getTitre());
			values.put(ARTISTE, m.getArtiste());
			values.put(GENRE, m.getGenre());
			values.put(ENVOI, (m.getEnvoi()) ? 1 : 0);
			values.put(PATH, m.getPath());
			values.put(MD5, m.getMd5());
			values.put(KEY, m.getId());
			//on ins�re l'objet dans la BDD via le ContentValues
			return bdd.insert(TABLE_NAME, null, values);
		  

	  }

	  /**

	   * @param md5 le md5 de la musique � supprimer
	 * @return 

	   */

	  public int supprimer(String md5) {

		 
				//Suppression d'une musique de la BDD gr�ce au md5
				return bdd.delete(TABLE_NAME, MD5 + "='" + md5 + "'", null);
			
	  }

	 

	  public int setId(String md5, int id) {
			ContentValues values = new ContentValues();
			values.put(KEY, id);
			return bdd.update(TABLE_NAME, values,  MD5 + " = ?" , new String[] {md5});

	  }
	  
	  public int setEnvoi(String md5, boolean envoi) {
			ContentValues values = new ContentValues();
			values.put(ENVOI, envoi? 1 : 0);
			return bdd.update(TABLE_NAME, values, MD5 + " = ?" , new String[] {md5});

	  }

	  /**

	   * @param id l'identifiant de la musique � r�cup�rer

	   */

	  public Musique selectionner(int id) {
		  Cursor c = bdd.query(TABLE_NAME, new String[] {TITRE, ARTISTE, GENRE, ENVOI,PATH,MD5,KEY}, KEY + " = \"" + id +"\"", null, null, null, null);
		//si aucun �l�ment n'a �t� retourn� dans la requ�te, on renvoie null
		  c.moveToFirst();
		  Musique m = cursortoMusique(c);
		  c.close();
		  return m;

	  }
	  
	  public Musique getMusiqueByMD5(String md5) {
		  Cursor c = bdd.query(TABLE_NAME, new String[] {TITRE, ARTISTE, GENRE,ENVOI, PATH,MD5,KEY}, MD5 + " LIKE \"" + md5 +"\"", null, null, null, null);
		//si aucun �l�ment n'a �t� retourn� dans la requ�te, on renvoie null
		  c.moveToFirst();
		  Musique m = cursortoMusique(c);
		  
		  c.close();
		  return m;

	  }
	  
	  public Musique cursortoMusique(Cursor c){
		  if (c.getCount() == 0)
				return null;
	 
		
			//On cr�� une musique en lui affectant toutes les infos gr�ce aux infos contenues dans le Cursor
			Musique m = new Musique(c.getString(NUM_COL_TITRE),c.getString(NUM_COL_ARTISTE),c.getString(NUM_COL_GENRE),c.getInt(NUM_COL_ENVOI)!=0,c.getString(NUM_COL_PATH),c.getString(NUM_COL_MD5),c.getInt(NUM_COL_KEY));
			
			//On retourne la musique
			return m;
	  }
	  
	  public List<Musique> getAllMusiques() {
		    List<Musique> comments = new ArrayList<Musique>();
		    Cursor cursor = bdd.query(TABLE_NAME, new String[] {TITRE, ARTISTE, GENRE, ENVOI,PATH,MD5,KEY}, null, null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      Musique comment = cursortoMusique(cursor);
		      comments.add(comment);
		      cursor.moveToNext();
		    }
		    //  fermeture du curseur
		    cursor.close();
		    return comments;
		  }
}
