package com.example.musicshared;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class InscriptionActivity extends Activity {
	
	
	private Button valider = null;
	EditText pseudo;
	EditText mdp;
	EditText mdpconf;
	EditText mail;
	String loginTxt;
	String mdpTxt;
	String mailTxt;
	String line;
	boolean pseudo_existe=true;
	boolean mail_existe=true;
	int code;
	GlobalVariables globalVariables;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inscription);
		globalVariables = (GlobalVariables) getApplicationContext();
		 pseudo = (EditText)findViewById(R.id.editText1);
		 mdp = (EditText)findViewById(R.id.editText2);
		 mdpconf = (EditText)findViewById(R.id.editText3);
		 mail = (EditText)findViewById(R.id.editText4);
		valider = (Button)findViewById(R.id.button1);
		valider.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				loginTxt = pseudo.getText().toString();			
				mdpTxt = mdp.getText().toString();
				final String mdpconfTxt = mdpconf.getText().toString();
				
				mailTxt = mail.getText().toString();
				
				if (loginTxt.equals("") || mailTxt.equals("") || mdpTxt.equals("") || mdpconfTxt.equals("")) {
					Toast.makeText(InscriptionActivity.this, R.string.case_empty,Toast.LENGTH_SHORT).show();
					valider.setEnabled(true);
					return;
					
				}
				
				if (!mdpTxt.equals(mdpconfTxt)){
					Toast.makeText(InscriptionActivity.this, R.string.erreur_mdp, Toast.LENGTH_SHORT).show();
					valider.setEnabled(true);
					return;
				}
				
				Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
				Matcher m = p.matcher(mailTxt);
				if (!m.matches()) {
					Toast.makeText(InscriptionActivity.this, R.string.email_format_error, Toast.LENGTH_SHORT).show();
					valider.setEnabled(true);
					return;
				}
				
				new check_pseudo().execute();
			}
		});
	}

	class check_pseudo extends AsyncTask<String, String, Void>{
		InputStream is = null;
		String result = "";
	       @Override
		protected Void doInBackground(String... params) {
	    	
	   		String result = "";
	   		int pseudo;
	   		// Envoyer la requ�te au script PHP.
	   		
	   		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	   		nameValuePairs.add(new BasicNameValuePair("pseudo",loginTxt));
	   		// Envoie de la commande http
	    	try
	    	{
			HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost("http://musicshared.netne.net/check_pseudo.php");
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost); 
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		        Log.e("pass 1", "connection success ");
	    	}
	        catch(Exception e)
	    	{
	        	Log.e("Fail 1", e.toString());
	    	}    

	   		// Conversion de la requête en string
	        try
	        {
	            BufferedReader reader = new BufferedReader
				(new InputStreamReader(is,"iso-8859-1"),8);
	            StringBuilder sb = new StringBuilder();
	            while ((line = reader.readLine()) != null)
		    {
	                sb.append(line + "\n");
	            }
	            is.close();
	            result = sb.toString();
		    Log.e("pass 2", "connection success ");
	        }
	        catch(Exception e)
	        {
	            Log.e("Fail 2", e.toString());
	        } 

		   	try
	    	{
	        	JSONObject json_data = new JSONObject(result);
	        	pseudo=Integer.parseInt((json_data.getString("pseudo")));
	        	if(pseudo!=0)
	        		pseudo_existe = true;
	        	else
	        		pseudo_existe = false;
	    	}
	        catch(Exception e)
	    	{    pseudo_existe = false;
	        	Log.e("Fail 3 - pseudo", e.toString());
	    	}
	   		return null;
		}
		protected void onPostExecute(Void v) {
			if (pseudo_existe){
			 Toast.makeText(getBaseContext(), "Le pseudo existe d�j�",Toast.LENGTH_SHORT).show();
			 valider.setEnabled(true);
			}
			else 
				new check_mail().execute();
	    }
	}
	
	class check_mail extends AsyncTask<String, String, Void>{
		InputStream is = null;
		String result = "";
	       @Override
		protected Void doInBackground(String... params) {
	    	
	   		String result = "";
	   		int mail;
	   		// Envoyer la requ�te au script PHP.
	   		// Script PHP : $sql=mysql_query("select * from tblVille where Nom_ville like '".$_REQUEST['ville']."%'");
	   		// $_REQUEST['pseudo'] sera remplac� par le pseudo dans notre exemple.
	   		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	   		nameValuePairs.add(new BasicNameValuePair("mail",mailTxt));
	   		// Envoie de la commande http
	    	try
	    	{
			HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost("http://musicshared.netne.net/check_mail.php");
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost); 
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		        Log.e("pass 1", "connection success ");
	    	}
	        catch(Exception e)
	    	{
	        	Log.e("Fail 1", e.toString());
	    	}    

	   		// Conversion de la requête en string
	        try
	        {
	            BufferedReader reader = new BufferedReader
				(new InputStreamReader(is,"iso-8859-1"),8);
	            StringBuilder sb = new StringBuilder();
	            while ((line = reader.readLine()) != null)
		    {
	                sb.append(line + "\n");
	            }
	            is.close();
	            result = sb.toString();
		    Log.e("pass 2", "connection success ");
	        }
	        catch(Exception e)
	        {
	            Log.e("Fail 2", e.toString());
	        } 

		   	try
	    	{
	        	JSONObject json_data = new JSONObject(result);
	        	mail=Integer.parseInt((json_data.getString("mail")));
	        	if(mail!=0)
	        		mail_existe = true;
	        	else
	        		mail_existe = false;
	    	}
	        catch(Exception e)
	    	{   mail_existe = false;
	        	Log.e("Fail 3 - mail", e.toString());
	    	}
	   		return null;
		}
		protected void onPostExecute(Void v) {
			if (mail_existe){
			 Toast.makeText(getBaseContext(), "L'adresse e-mail existe d�j�",Toast.LENGTH_SHORT).show();
			 valider.setEnabled(true);
			}
			else{
			//	infos.setText("pseudo_existe : " + pseudo_existe + "\n mail_existe : " + mail_existe );
				if (!pseudo_existe&&!mail_existe){
					new insert().execute();
				}
			}
		
	    }
	}
	
	
	class insert extends AsyncTask<String, String, Void>{
		InputStream is = null;
		String result = "";
	       @Override
		protected Void doInBackground(String... params) {
	    	   
	   		String result = "";
	   		
	   		// Envoyer la requ�te au script PHP.
	   		
	   		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	   		nameValuePairs.add(new BasicNameValuePair("pseudo",loginTxt));
	   		nameValuePairs.add(new BasicNameValuePair("mdp",mdpTxt));
	   		nameValuePairs.add(new BasicNameValuePair("mail",mailTxt));
	   		
	   		

	   		// Envoie de la commande http
	    	try
	    	{
			HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost("http://musicshared.netne.net/ajout_utilisateur.php");
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost); 
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		        Log.e("pass 1", "connection success ");
	    	}
	        catch(Exception e)
	    	{
	        	Log.e("Fail 1", e.toString());
		    	
	    	}    

	   		// Conversion de la requ�te en string
	        try
	        {
	            BufferedReader reader = new BufferedReader
				(new InputStreamReader(is,"iso-8859-1"),8);
	            StringBuilder sb = new StringBuilder();
	            while ((line = reader.readLine()) != null)
		    {
	                sb.append(line + "\n");
	            }
	            is.close();
	            result = sb.toString();
		    Log.e("pass 2", "connection success ");
	        }
	        catch(Exception e)
	        {
	            Log.e("Fail 2", e.toString());
	        } 
	        try
			{
		            JSONObject json_data = new JSONObject(result);
		            code=(json_data.getInt("code"));
					
		           
			}
			catch(Exception e)
			{
		            Log.e("Fail 3", e.toString());
			}
	   		return null;
		}
		protected void onPostExecute(Void v) {
			 if(code==1)
	            {
			Toast.makeText(getBaseContext(), "Inscreption r�ussie",
				Toast.LENGTH_SHORT).show();
				globalVariables.setPseudo(loginTxt);
				new get_id().execute();
	            }
	            else
	            {
	            	Toast.makeText(getBaseContext(), "Inscription �chou�e",Toast.LENGTH_LONG).show();
	            	Log.e("insert", "failure");
	            }
			
	    }
	

	
	}
	
	//recupere l'id du nouvel inscrit
	class get_id extends AsyncTask<String, String, Void>{
		InputStream is = null;
		String result = "";
	       @Override
		protected Void doInBackground(String... params) {
	    	   
	   		String result = "";
	   		
	   		// Envoyer la requ�te au script PHP.
	   		
	   		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	   		nameValuePairs.add(new BasicNameValuePair("pseudo",globalVariables.getPseudo()));
	   		
	   		

	   		// Envoie de la commande http
	    	try
	    	{
			HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost("http://musicshared.netne.net/get_id_from_pseudo.php");
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost); 
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		        Log.e("pass 1", "connection success ");
	    	}
	        catch(Exception e)
	    	{
	        	Log.e("Fail 1", e.toString());
	    	}    

	   		// Conversion de la requ�te en string
	        try
	        {
	            BufferedReader reader = new BufferedReader
				(new InputStreamReader(is,"iso-8859-1"),8);
	            StringBuilder sb = new StringBuilder();
	            while ((line = reader.readLine()) != null)
		    {
	                sb.append(line + "\n");
	            }
	            is.close();
	            result = sb.toString();
		    Log.e("pass 2", "connection success ");
	        }
	        catch(Exception e)
	        {
	            Log.e("Fail 2", e.toString());
	        } 
	        try
			{
		            JSONObject json_data = new JSONObject(result);
		            globalVariables.setId(json_data.getInt("id")); 
					
		           
			}
			catch(Exception e)
			{
		            Log.e("Fail 3", e.toString());
			}
	   		return null;
		}
		protected void onPostExecute(Void v) {
				valider.setEnabled(true);
				Intent accueil = new Intent(InscriptionActivity.this, ListeActivity.class);
		    	   startActivity(accueil);
	       	
	    }
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
		case R.id.action_settings:
			Intent intent = new Intent(InscriptionActivity.this, ParametreActivity.class);
			startActivity(intent);		
			return true;
		case R.id.action_disconnection:			
			finish();
			return true;
		default:	
			return super.onOptionsItemSelected(item);
		}		
	}
}
