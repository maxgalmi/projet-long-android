package com.example.musicshared;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;




public class ServiceInvisible extends Service {

	BluetoothAdapter blueAdapt;
	BluetoothReceiver br;
	String id="00001101-0000-1000-8000-00805F9B34FB";
	boolean trouve_expediteur = false;
	boolean trouve_recepteur = false;
	int nbThread;
	String titremusic=null;
	long taillefichier;
	boolean attentethread;
	boolean activationReceiver;
	MusiqueDAO bdd;
	GlobalVariables globalVariables;
	int icon;
	int num_notification = 2;
	public int onStartCommand(Intent i,int flags,int startId){
		Log.e("debut","debut");
		activationReceiver=false;
		nbThread=0;
		
		attentethread=false;
		bdd = new MusiqueDAO(this);
        bdd.open();
    	Log.e("debut","debut");
    	globalVariables = (GlobalVariables) getApplicationContext();
    	icon =R.drawable.ic_launcher ;
		// Le premier titre affich�
		CharSequence titre = "Titre de la notification";
		// Dat� de maintenant
		long when = System.currentTimeMillis();
		Intent notificationIntent = new Intent(this, AccueilActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		
		// La notification est cr��e
		Notification notification = new Notification(icon,titre, when);
		
		notification.setLatestEventInfo(this, "Musicshared", "activ�e", contentIntent);
		
		this.startForeground(1, notification);
   

		blueAdapt=BluetoothAdapter.getDefaultAdapter();
		if((!blueAdapt.isEnabled())){
			blueAdapt.enable();
		}

		br=new BluetoothReceiver();
		activerReceiver();
		


    blueAdapt.startDiscovery();		
    	Log.e("debut","debut");
   
    
    /*Intent discoverableIntent = new  Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
	 discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,500);
	 discoverableIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
startActivity(discoverableIntent);*/
    	
GlobalVariables.startMyTask(new BluetoothServer(blueAdapt));
		
		
		return super.onStartCommand(i, flags, startId);
		
		
		
		
	}
	
	
			 public void activerReceiver(){
				 IntentFilter filtre = new IntentFilter();
					filtre.addAction(BluetoothDevice.ACTION_FOUND);
					filtre.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
					filtre.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
					filtre.addAction(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED);
			    	Log.e("activationreceiver","activerreceiver()");


			    
			    	registerReceiver(br, filtre);
			    	activationReceiver=true;

			 }
			 public void desactiverReceiver(){
			    	unregisterReceiver(br);
			    	Log.e("desactivationreceiver","desactiverreceiver()");
			    	activationReceiver=false;
			 }
	public class BluetoothReceiver extends BroadcastReceiver{
		 @Override 
		 public void onReceive(Context context, Intent arg1) { 
		 // TODO Auto-generated method stub 
			 String action = arg1.getAction();
		    	Log.e("bluetoothreceiver","receiver message");
		    	if(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED.equals(action)){
			    	Log.e("bluetoothreceiver","receive scan mod change");

					 int scanmode = arg1.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,-1);
					 int ancienscanmode = arg1.getIntExtra(BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE,-1);

			           if ((scanmode==BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)&&(ancienscanmode!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)) {
			        		Log.e( "bluetoothreceiver" ," bluetooth visible");

			            } else if ((scanmode!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)&&(ancienscanmode==BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE)) {
			        		Log.e( "bluetoothreceiver" ," fin bluetooth visible");
			        		Intent discoverableIntent = new  Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
			  			  discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION,3600);
			  			  startActivity(discoverableIntent);
			  			 
			            } 
			
					 }
		    	else{

	            if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
			    	Log.e("bluetoothreceiver","receive discovery started");


	            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action) && !trouve_expediteur && !trouve_recepteur) {
			    	Log.e("bluetoothreceiver","receive discovery finish");

	        		blueAdapt.startDiscovery();

	            } else if (BluetoothDevice.ACTION_FOUND.equals(action) && !trouve_expediteur && !trouve_recepteur && !attentethread) {

			    	Log.e("bluetoothreceiver","found");

	                //bluetooth device found
	            	trouve_expediteur=true;
	        		 BluetoothDevice device = arg1.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

	        		//Toast.makeText(context, "nouveau mobile bluetooth detect� "+device.getName() ,Toast.LENGTH_SHORT ).show();
	        		ConnectThread ct=new ConnectThread(device);
	        		GlobalVariables.startMyTask(ct);
	        		
	              
	      
	            }
	        }

		 } 
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void onDestroy(){
		this.stopForeground(true);
		super.onDestroy();
		blueAdapt.cancelDiscovery();
		if(activationReceiver){
			desactiverReceiver();
		}
Log.e("fin de service"," ");		
		
		
	}
	
	
	
	private class ConnectThread extends AsyncTask<Void,String,Void>{
		   private  BluetoothSocket bSocket;
		   private  BluetoothDevice tmp;
		  boolean recu;
		   public ConnectThread(BluetoothDevice device) {
		      tmp = device;
		      Log.e("Connecthread",tmp.getName()+" "+tmp.getAddress());
		  recu=false;
		      
		   }

		   

		   public void cancel() {
		      try {
		          bSocket.close();
		          if(!blueAdapt.isDiscovering()&&!trouve_recepteur){
						blueAdapt.startDiscovery();
					}
		          if(!activationReceiver){
		        	  activerReceiver();
		          }
			      Log.e("Connecthread","la fermeture de la socket a reussit");

		       } catch (IOException e) { 
				      Log.e("Connecthread","la fermeture de la socket a echoue");

		       }
		      if(trouve_expediteur){
		    	  trouve_expediteur = false;
		    	  //gerertrouve(false);
		      }
		      if(recu){
		      attentethread=true;
		      Log.e("conecthread","lancementattentethread");
		      try {
				Thread.sleep(300);
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		      Log.e("conecthread","lancementattentethread");

		      attentethread=false;
		      }
		   }
		   protected void onProgressUpdate(String... values){
		          
	            // Mise � jour de la ProgressBar
	           Toast.makeText(getApplicationContext(), values[0] ,Toast.LENGTH_SHORT).show();

	        }
		
		@Override
		protected Void doInBackground(Void... params) {
		      try {
		    	  while(globalVariables.getScan()&&globalVariables.getMaj()){Thread.sleep(100);}
			        bSocket = tmp.createRfcommSocketToServiceRecord(UUID.fromString(id));
			        bSocket.connect();
			        byte []b=new byte[100];
				    OutputStream out=bSocket.getOutputStream();
				    InputStream in=bSocket.getInputStream();
					FonctionsTransfert.ecrireTexte2(out,"musique");
					blueAdapt.cancelDiscovery();
					if(activationReceiver) desactiverReceiver();
					int reponse=FonctionsTransfert.discussionExpediteur(in, out, bdd.getAllMusiques());
					if(reponse==-1) cancel();
					else{
						Musique m = bdd.selectionner(reponse);
						File f = new File(m.getPath());
						FonctionsTransfert.ecrireTexte2(out,f.getName());
						FonctionsTransfert.ecrireTexte2(out,m.getTitre());
						FonctionsTransfert.ecrireTexte2(out,m.getArtiste());
						if(m.genre==null) FonctionsTransfert.ecrireTexte2(out,"");
						else FonctionsTransfert.ecrireTexte2(out,m.getGenre());
						//FonctionsTransfert.ecrireInt(out,m.getId());
						FonctionsTransfert.ecrireTexte2(out,m.getMd5());
						SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ServiceInvisible.this);
						Boolean ano = pref.getBoolean("anonyme", false);
						if(ano) FonctionsTransfert.ecrireTexte2(out, "Anonyme");
						else FonctionsTransfert.ecrireTexte2(out, globalVariables.getPseudo());
						String pseudo=FonctionsTransfert.lireTexte2(in);
						publishProgress("Envoi de musique en cours...");
						FonctionsTransfert.transmissionFichier(out,m.getPath());  
						publishProgress("Musique envoy�");
						MusiqueEnvoyeRecu mer = new MusiqueEnvoyeRecu(m,pseudo,false);
						globalVariables.addMusiqueER(mer);
						FileOutputStream fos = openFileOutput("MusiquesER_"+globalVariables.getPseudo(), Context.MODE_PRIVATE);
						ObjectOutputStream os2 = new ObjectOutputStream(fos);
						os2.writeObject(globalVariables.getMusiquesER());
						os2.close();
						fos.close();	  
					}
						    
						    
						    Thread.sleep(15000);
						      Log.e("Connecthread","finsleep ");
						      
						      out.close();
						      recu=true;
			      // }
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
					}catch (IOException e) { 
					      Log.e("Connecthread","connection echoue "+tmp.getName()+" "+e.toString());

			    	   cancel();
			       }catch (Cancel e) { 
					      Log.e("Connecthread","connection echoue "+tmp.getName()+" "+e.toString());

			    	   cancel();
			       }
					      
		      
		      cancel();
			return null;
		}


	
	}
	
	
	
	
	
		public class BluetoothServer extends AsyncTask<Void,String,Void> {
	    private  BluetoothServerSocket bss;
	    private BluetoothAdapter blueAdapter;
		String recu;
		String pseudo; //pseudo de l'expediteur;
		boolean musique_recu = false;
	    public BluetoothServer(BluetoothAdapter blueAdapter) {
	    	this.blueAdapter=blueAdapter;
	        // On utilise un objet temporaire qui sera assign� plus tard � blueServerSocket car blueServerSocket est "final"
	    	Log.e("blueserver","creation onbjet");
	    }

	    
	    
	   @Override
	   protected void onPostExecute(Void result) {
		   //Toast.makeText(getApplicationContext(), "post" ,Toast.LENGTH_SHORT).show();
		   if(musique_recu){
		   Toast.makeText(getApplicationContext(), "Musique re�ue !" ,Toast.LENGTH_SHORT).show();
			final NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE); 
			Intent notificationIntent = new Intent(ServiceInvisible.this, ListeActivity.class);
			notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
			PendingIntent contentIntent = PendingIntent.getActivity(ServiceInvisible.this,1, notificationIntent, 0);
			
			NotificationCompat.Builder builder = new NotificationCompat.Builder(ServiceInvisible.this)
			.setWhen(System.currentTimeMillis())
			.setTicker("Musicshared")
			.setSmallIcon(icon)
			.setContentTitle("Musicshared")
			.setContentText("Musique re�ue de " + pseudo + " !")
			.setContentIntent(contentIntent);
			notificationManager.notify(num_notification++, builder.build()); 
		   }
			// La notification est cr��e
		
			
		   try {
			   bss.close();
			   if(!blueAdapt.isDiscovering()){
					blueAdapt.startDiscovery();
				}
	          if(!activationReceiver){
	        	  activerReceiver();
	          }
			   
		    	Log.e("onpostexecute","post fermeture");
			   if(trouve_recepteur){
				   trouve_recepteur = false;
			   //gerertrouve(false);
			   }
			   GlobalVariables.startMyTask(new BluetoothServer(blueAdapt));

		   } catch (IOException e) {
			   // TODO Auto-generated catch block
		    	Log.e("onpostexecute","post fermeture rater");

			   e.printStackTrace();
		   }
	   }
	   @Override
	   protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			Log.e("asynctask","do");
			try {
			   while(globalVariables.getScan()&&globalVariables.getMaj()){ Thread.sleep(100);}
				bss= this.blueAdapter.listenUsingRfcommWithServiceRecord("MusicShared", UUID.fromString(id));
			//	publishProgress("creation de socket qui ecoute");
				BluetoothSocket bs=bss.accept(); trouve_recepteur = true;
				if(activationReceiver) desactiverReceiver(); blueAdapt.cancelDiscovery();
				//publishProgress("connexion reussie");
				InputStream in=bs.getInputStream();
				OutputStream out=bs.getOutputStream();
				//publishProgress("debut lecture");
				recu=FonctionsTransfert.lireTexte2(in);
				if(recu.equals("musique")){
				      int  reponse=FonctionsTransfert.discussionRecepteur(in, out,globalVariables.getSuggestions());
						    if(reponse!=-1){
								String nomfichier=FonctionsTransfert.lireTexte2(in);
								publishProgress("Musique trouv� - "+nomfichier);
								File folder = new File(Environment.getExternalStorageDirectory() +"/MusicShared/");
								if(!folder.exists()) folder.mkdirs();
								publishProgress("Reception de musique en cours...");
								String path = Environment.getExternalStorageDirectory() +"/MusicShared/" + nomfichier;
								String titre=FonctionsTransfert.lireTexte2(in);
								String artiste=FonctionsTransfert.lireTexte2(in);
								String genre=FonctionsTransfert.lireTexte2(in);
								//int id=FonctionsTransfert.lireInt(in);
								String md5 = FonctionsTransfert.lireTexte2(in);
								pseudo=FonctionsTransfert.lireTexte2(in);
								SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(ServiceInvisible.this);
								Boolean ano = pref.getBoolean("anonyme", false);
								if(ano) FonctionsTransfert.ecrireTexte2(out, "Anonyme");
								else FonctionsTransfert.ecrireTexte2(out, globalVariables.getPseudo());
								FonctionsTransfert.receptionFichier(in,path);
								Musique m = new Musique(titre,artiste,genre,true,path,md5,reponse);
								globalVariables.addNew(m); bdd.ajouter(m);
								MusiqueEnvoyeRecu mer = new MusiqueEnvoyeRecu(m,pseudo,true);
								globalVariables.addMusiqueER(mer);
								FileOutputStream fos = openFileOutput("MusiquesER_"+globalVariables.getPseudo(), Context.MODE_PRIVATE);
								ObjectOutputStream os2 = new ObjectOutputStream(fos);
								os2.writeObject(globalVariables.getMusiquesER()); os2.close(); fos.close();
								globalVariables.getSuggestions().remove(globalVariables.appartient_suggestion(reponse));
								musique_recu = true;
							}
				}
		       } catch (IOException e) {
				      Log.e("bluetoothserver","connection echoue ");
						//publishProgress("erreur de connexion"+e.toString());
						//Toast.makeText(getApplicationContext(), "erreur de connexion:"+e.toString() ,Toast.LENGTH_LONG).show();

				//publishProgress("fin de connexion");

						return null;
			
		} catch (Cancel e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return null;
			}
		 @Override
	        protected void onProgressUpdate(String... values){
	          
	            // Mise � jour de la ProgressBar
	           Toast.makeText(getApplicationContext(), values[0] ,Toast.LENGTH_SHORT).show();

	        }
		
		@Override
		 protected void onPreExecute() {
Log.e("asynctask","pre");

		     }

	
			
}
		//public synchronized void gerertrouve(boolean b){
		//	trouve=b;
		//}
		
		
}
	
	



