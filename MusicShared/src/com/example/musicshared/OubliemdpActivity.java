package com.example.musicshared;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class OubliemdpActivity extends Activity {
	
	private Button valider = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_oubliemdp);
		
		valider = (Button)findViewById(R.id.button1);
		valider.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final EditText mail = (EditText)findViewById(R.id.editText1);
				final String mailTxt = mail.getText().toString();
				if(mailTxt.equals("")){
					Toast.makeText(OubliemdpActivity.this, R.string.case_empty, Toast.LENGTH_SHORT).show();
					return;
				}
				Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
				Matcher m = p.matcher(mailTxt);
				if (!m.matches()) {
					Toast.makeText(OubliemdpActivity.this, R.string.email_format_error, Toast.LENGTH_SHORT).show();
					return;
				}
				
				Intent intent = new Intent(OubliemdpActivity.this, ConnexionActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				startActivity(intent);
			}
			
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){
		case R.id.action_settings:
			Intent intent = new Intent(OubliemdpActivity.this, ParametreActivity.class);
			startActivity(intent);		
			return true;
		case R.id.action_disconnection:			
			finish();
			return true;
		default:	
			return super.onOptionsItemSelected(item);
		}
	}
}
