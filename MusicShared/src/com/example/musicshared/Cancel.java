package com.example.musicshared;

public class Cancel extends Exception{
	public Cancel(String message){
		super("CancelException:"+message);
	}
	public String toString(){
		return getMessage();
	}

}
