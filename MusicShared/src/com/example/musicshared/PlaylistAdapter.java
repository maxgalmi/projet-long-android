package com.example.musicshared;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PlaylistAdapter extends ArrayAdapter<Musique> {
	private Context c;
	private int id;
	private List<Musique>items;
	public PlaylistAdapter(Context context,
			int textViewResourceId, List<Musique> objects) {
		super(context, textViewResourceId, objects);
		c = context;
		id = textViewResourceId;
		items = objects;
	}
	
	public Musique getItem(int i)
	 {
		 return items.get(i);
	 }
	
	 @Override
     public View getView(int position, View convertView, ViewGroup parent) {
             View v = convertView;
             if (v == null) {
                 LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                 v = vi.inflate(id, null);
             }
            // CheckBox cBox = (CheckBox) v.findViewById(R.id.chk);
            
             final Musique o = items.get(position);
             if (o != null) {
                 TextView t1 = (TextView) v.findViewById(R.id.TextView03);
                 TextView t2 = (TextView) v.findViewById(R.id.TextView04);
                
                 if(t1!=null)
                 	t1.setText(o.getTitre() + " - id : "+ o.getId());
                 if(t2!=null)
                 	t2.setText(o.getArtiste());
         }
            
            
             return v;
     }

}
