package com.example.musicshared;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Application;
import android.os.AsyncTask;
import android.os.Build;



public class GlobalVariables extends Application{
	String pseudo;
	int id_utilisateur;
	List<Integer> suggestions;
	boolean scan_en_cours = false;
	boolean majbdd_en_cours = false;
	List<Musique> nouvelles_musiques;
	List<MusiqueEnvoyeRecu> musiquesER;
	
	
	
	public String getPseudo(){
		return pseudo;
	}
	
	public void setPseudo(String p){
		pseudo  = p;
	}
	
	
	public List<Integer> getSuggestions(){
		return suggestions;	
	}
	
	public void viderSuggestions(){
		suggestions = new ArrayList<Integer>();
	}
	
	public void addSuggestion(int n){
		suggestions.add(n);	
	}
	
	public void addMusiqueER(MusiqueEnvoyeRecu m){
		if (musiquesER==null) viderMusiqueER();  
		musiquesER.add(m);	
	}
	
	public List<MusiqueEnvoyeRecu> getMusiquesER(){
		if (musiquesER==null)
			return new ArrayList<MusiqueEnvoyeRecu>();
		return musiquesER;
	}
	public void  setMusiquesER(List<MusiqueEnvoyeRecu> m){
		musiquesER = m;
	}
	
	
	public void viderMusiqueER(){
		musiquesER = new ArrayList<MusiqueEnvoyeRecu>();
	}
	
	public void setSuggestions(List<Integer> s){
		suggestions = s;
	}
	
	public boolean getScan(){
		return scan_en_cours ;
	}
	
	public void setScan(boolean b){
		scan_en_cours = b;
	}
	
	public boolean getMaj(){
		return majbdd_en_cours ;
	}
	
	public void setMaj(boolean b){
		majbdd_en_cours = b;
	}
	
	
	public int getId(){
		return id_utilisateur;	
	}
	
	public void setId(int i){
		id_utilisateur = i;
	}
	
	
	public List<Musique> getNew(){
		if(nouvelles_musiques == null) viderNew();
		return nouvelles_musiques;
	}
	
	public void viderNew(){
		nouvelles_musiques = new ArrayList<Musique>();
	}
	public void addNew(Musique m){
		if(nouvelles_musiques == null) viderNew();
		nouvelles_musiques.add(m);	
	}
	public void get_nouvelles_musiques(List<Musique> dir, List<Musique> ancienne_playlist){
		nouvelles_musiques = new ArrayList<Musique>();
		
		for (int i = 0; i<dir.size();i++){
			if (!dir.get(i).appartient(ancienne_playlist)){
				nouvelles_musiques.add(dir.get(i));
			}
		}
		
		
		//return nouvelles_musiques;
		
		
	}
	
	
	
	
	public   int appartient_suggestion(int n){
		for(int i = 0;i<suggestions.size();i++){
			if(suggestions.get(i)==n)
				return i;
		}
			
		return -1;
		
	}
	public static  int appartient(int n, List<Musique> l){
		for(int i = 0;i<l.size();i++){
			if(l.get(i).getId()==n)
				return i;
		}
			
		return -1;
		
	}
	
	
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void startMyTask(AsyncTask asyncTask){
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
			asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,null);
		else
			asyncTask.execute();
	}
	
	 public static Comparator<Musique> titreOrder =  new Comparator<Musique>() {
        public int compare(Musique m1, Musique m2) {
            return m1.getTitre().toLowerCase().compareTo(m2.getTitre().toLowerCase());
        }
    };
    
    public static Comparator<Musique> artisteOrder =  new Comparator<Musique>() {
        public int compare(Musique m1, Musique m2) {
            return m1.getArtiste().toLowerCase().compareTo(m2.getArtiste().toLowerCase());
        }
    };
    
    public  static Comparator<Musique> genreOrder =  new Comparator<Musique>() {
        public int compare(Musique m1, Musique m2) {
        	if(m1.getGenre()!=null&&m2.getGenre()!=null)
            return m1.getGenre().toLowerCase().compareTo(m2.getGenre().toLowerCase());
        	else if(m1.getGenre()==null&&m2.genre==null)
        		return 0;
        	else if(m1.genre==null)
        		return -1;
        	else return 1;
        }
    };
    
  
	
}
