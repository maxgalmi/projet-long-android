package com.example.musicshared;

import java.util.prefs.Preferences;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class ParametreActivity extends PreferenceActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		setContentView(R.layout.main);
		Preference envoi = (Preference) findPreference("envoi");
		envoi.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
		    @Override
		    public boolean onPreferenceClick(Preference arg0) {
		        Intent intent = new Intent(ParametreActivity.this, ListeActivity.class);
		        intent.putExtra("parametres", true);
		        startActivity(intent);
		        return true;
		    }
		});

	}

	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
