package com.example.musicshared;



import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class BaseSQLite extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	 private static final String TABLE_NAME = "musiques";

	  private static final String KEY = "id";

	  private static final String TITRE = "titre";

	  private static final String ARTISTE = "artiste";
	  
	  private static final String MD5 = "md5";
	  private static final String GENRE = "genre";
	  private static final String PATH = "path";
	  private static final String ENVOI = "envoi";
	  private static final String CREATE_BDD =
			  "CREATE TABLE " + TABLE_NAME + " (" +
					     MD5 + " TEXT PRIMARY KEY , " +
					      TITRE + " TEXT NOT NULL, " +
					      ARTISTE + " TEXT NOT NULL, " +
					      GENRE + " TEXT, " +
					      PATH + " TEXT NOT NULL, " +
					      ENVOI + " INTEGER NOT NULL, " +
					      KEY + " INTEGER);";
			
		
 
	public BaseSQLite(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {
		//on cr�� la table � partir de la requ�te �crite dans la variable CREATE_BDD
		db.execSQL(CREATE_BDD);
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE " + TABLE_NAME + ";");
		onCreate(db);
	}

}
