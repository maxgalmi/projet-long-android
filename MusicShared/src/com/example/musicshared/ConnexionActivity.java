package com.example.musicshared;

//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
//import android.widget.EditText;
import android.widget.TextView;
//import android.widget.Toast;
import android.widget.Toast;

public class ConnexionActivity extends Activity implements OnTouchListener{
	
	private Button enterButton;
	private TextView forgetpwd;
	private TextView newmember;
	boolean success = false;
	String passTxt;
	String loginTxt;
	CheckBox connexion_auto;
	
	 GlobalVariables globalVariables;
	;
	
	private OnClickListener clickListenerEnter = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			final EditText login = (EditText)findViewById(R.id.editText3);
			final EditText passwd = (EditText)findViewById(R.id.editText2);
			loginTxt = login.getText().toString();
			passTxt = passwd.getText().toString();
					
			if (loginTxt.equals("") || passTxt.equals("")) {
				Toast.makeText(ConnexionActivity.this, R.string.case_empty,Toast.LENGTH_SHORT).show();
				
			}
			else{			
			
				new connexion().execute();
			}
	    }
	};
	private OnClickListener clickListenerForget = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(ConnexionActivity.this, OubliemdpActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			startActivity(intent);
	    }
	};
	
	private OnClickListener clickListenerNewMember = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Intent intent = new Intent(ConnexionActivity.this, InscriptionActivity.class);
			startActivity(intent);
		}
	};
	
	
	class connexion extends AsyncTask<String, String, Void>{
		InputStream is = null;
		String result = "";
		String line;
		String pseudo;
		String mdp;
		int id_utilisateur;
		
		
	       @Override
		protected Void doInBackground(String... params) {
	    	
	   		String result = "";
	   		
	   		// Envoyer la requ�te au script PHP.
	 
	   		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
	   		nameValuePairs.add(new BasicNameValuePair("pseudo",loginTxt));
	   		nameValuePairs.add(new BasicNameValuePair("mdp",passTxt));
	   		// Envoie de la commande http
	    	try
	    	{
			HttpClient httpclient = new DefaultHttpClient();
		        HttpPost httppost = new HttpPost("http://musicshared.netne.net/connexion.php");
		        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		        HttpResponse response = httpclient.execute(httppost); 
		        HttpEntity entity = response.getEntity();
		        is = entity.getContent();
		        Log.e("pass 1", "connection success ");
	    	}
	        catch(Exception e)
	    	{
	        	Log.e("Fail 1", e.toString());
	    	}    

	   		// Conversion de la requête en string
	        try
	        {
	            BufferedReader reader = new BufferedReader
				(new InputStreamReader(is,"iso-8859-1"),8);
	            StringBuilder sb = new StringBuilder();
	            while ((line = reader.readLine()) != null)
		    {
	                sb.append(line + "\n");
	            }
	            is.close();
	            result = sb.toString();
		    Log.e("pass 2", "connection success ");
	        }
	        catch(Exception e)
	        {
	            Log.e("Fail 2", e.toString());
	        } 

		   	try
	    	{
	        	JSONObject json_data = new JSONObject(result);
	        	id_utilisateur = json_data.getInt("id");
	        	pseudo=(json_data.getString("pseudo"));
	        	mdp=(json_data.getString("mdp"));
	        	success = true;
	    	}
	        catch(Exception e)
	    	{   success = false;
	        	Log.e("Fail 3 - connexion", e.toString());
	    	}
	   		return null;
		}
		protected void onPostExecute(Void v) {
			if (!success){
			 Toast.makeText(getBaseContext(), "Pseudo ou mot de passe incorrect",Toast.LENGTH_SHORT).show();
			 enterButton.setEnabled(true);
			}
			else{
				Toast.makeText(getBaseContext(), "Connexion �tablie",Toast.LENGTH_SHORT).show();
				globalVariables.setPseudo(loginTxt); 
				globalVariables.setId(id_utilisateur);
				if (connexion_auto.isChecked()){
					SharedPreferences preferences = getSharedPreferences("prefName", MODE_PRIVATE);
					  SharedPreferences.Editor editor = preferences.edit();
					  editor.putBoolean("connexion_auto", true);
					  editor.putInt("id_utilisateur", id_utilisateur);
					  editor.putString("pseudo", loginTxt);
					  editor.putString("mdp", passTxt);
					  editor.commit();
				}
				
				Intent liste = new Intent(ConnexionActivity.this, ListeActivity.class);
				liste.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);		
				startActivity(liste);
			}
		
	    }
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connexion);
		globalVariables = (GlobalVariables) getApplicationContext();
		enterButton = (Button)findViewById(R.id.button2);
		forgetpwd = (TextView)findViewById(R.id.textView2);
		newmember = (TextView)findViewById(R.id.textView1);
		enterButton.setOnClickListener(clickListenerEnter);
		forgetpwd.setOnClickListener(clickListenerForget);
		newmember.setOnClickListener(clickListenerNewMember);
		connexion_auto = (CheckBox)findViewById(R.id.checkBox1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch(id){

		case R.id.action_disconnection:			
			finish();
			return true;
		default:	
			return super.onOptionsItemSelected(item);
		}
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}
}
