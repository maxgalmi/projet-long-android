package com.example.musicshared;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;

public class FileArrayAdapter extends ArrayAdapter<Musique> implements OnCheckedChangeListener {
	private Context c;
	private int id;
	private List<Musique>items;
	public boolean[] checked;
	public FileArrayAdapter(Context context, int textViewResourceId,
			List<Musique> objects) {
		super(context, textViewResourceId, objects);
		c = context;
		id = textViewResourceId;
		items = objects;
		checked= new boolean[objects.size()];
		for (int i = 0;i<objects.size();i++){
			checked[i] = false;
		}
	
	}
	public Musique getItem(int i)
	 {
		 return items.get(i);
	 }
	 @Override
       public View getView(int position, View convertView, ViewGroup parent) {
               View v = convertView;
               if (v == null) {
                   LayoutInflater vi = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                   v = vi.inflate(id, null);
               }
               CheckBox cBox = (CheckBox) v.findViewById(R.id.chk);
               cBox.setTag(Integer.valueOf(position)); // set the tag so we can identify the correct row in the listener
               cBox.setChecked(checked[position]); // set the status as we stored it        
               cBox.setOnCheckedChangeListener(this); // set the listener  
               final Musique o = items.get(position);
               if (o != null) {
                       TextView t1 = (TextView) v.findViewById(R.id.TextView01);
                       TextView t2 = (TextView) v.findViewById(R.id.TextView02);
                      
                       if(t1!=null)
                       	t1.setText(o.getTitre());
                       if(t2!=null)
                       	t2.setText(o.getArtiste());
                       if (o.getEnvoi())
                    	   cBox.setChecked(true); 
               }
               
              
               return v;
       }
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		checked[(Integer)buttonView.getTag()] = isChecked;
	}

}
