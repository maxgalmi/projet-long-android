import java.net.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.musicshared.*;



@SuppressWarnings("deprecation")
public class SimpleServer extends Thread{
	int id; //id_utilisateur
	Socket socket;
	final static int port = 6660;
	public SimpleServer(Socket s){
		socket = s;
	}
	
	@SuppressWarnings("unchecked")
	public void run(){
		System.out.println("Client connect� !");
		
		
		
		InputStream is;
		try {
			is = socket.getInputStream();
			sleep(5000);
		ObjectInputStream ois = new ObjectInputStream(is);
		//reception du pseudo
		id = (Integer)ois.readObject();
		
			//reception de la playlist de l'utilsateur
			List<Musique> user_playlist = (List<Musique>)ois.readObject();
			System.out.println("Playlist : ");
			for (int i=0;i<user_playlist.size();i++){
				System.out.println(user_playlist.get(i).getTitre() + " - " + user_playlist.get(i).getArtiste());
			}
			List<Musique> musiquesbdd= get_musiques();
			int musiquesajoute_musiques=0;
			int musiquesajoute_possede=0;
			List<Musique> musiquesaajouter_musiques = new ArrayList<Musique>(); 
    		List<Musique> musiquesaajouter_possede = new ArrayList<Musique>();
    		for(int i=0;i<user_playlist.size();i++){
    			//Musique sans id = non pr�sente dans la base de donn�es externe
    			if(user_playlist.get(i).getId()==-1){
    				boolean trouve = false;
    				for(int j=0;j<musiquesbdd.size();j++){
    					if(StringSimilarity.similarity(user_playlist.get(i).getTitre(), musiquesbdd.get(j).getTitre())>0.9
    						&& StringSimilarity.similarity(user_playlist.get(i).getArtiste(), musiquesbdd.get(j).getArtiste())>0.9){
    						System.out.println("found : " + user_playlist.get(i).getTitre());
    						user_playlist.get(i).setTitre(musiquesbdd.get(j).getTitre());
    						user_playlist.get(i).setArtiste(musiquesbdd.get(j).getArtiste());
    						//maj de l'id dans la playlist
    						user_playlist.get(i).setId(musiquesbdd.get(j).getId());
    						//bdd.setId(user_playlist.get(i).getMd5(),musiquesbdd.get(j).getId());
    						musiquesaajouter_possede.add(user_playlist.get(i));
    						trouve = true;
    						break;
    					}
    				}
    					if(!trouve){
    						System.out.println("not found : " + user_playlist.get(i).getTitre());
    						musiquesaajouter_musiques.add(user_playlist.get(i));
    						musiquesaajouter_possede.add(user_playlist.get(i));
    					}
    				}
    				
    			}
    		for(int i=0;i<musiquesaajouter_musiques.size();i+=10){
    			List<Musique> list = new ArrayList<Musique>();
    			for (int j=i;j<i+10&&j<musiquesaajouter_musiques.size();j++){
    				list.add(musiquesaajouter_musiques.get(j));
    			}
    			musiquesajoute_musiques+= add_musiques(list);
    		}
    		
    		for(int i=0;i<musiquesaajouter_possede.size();i+=10){
    			List<Musique> list = new ArrayList<Musique>();
    			for (int j=i;j<i+10&&j<musiquesaajouter_possede.size();j++){
    				list.add(musiquesaajouter_possede.get(j));
    			}
    			musiquesajoute_possede += add_possede(list,id);
    		}
    		
    			
    
		    
		    OutputStream os = socket.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(os);
			oos.writeObject(musiquesajoute_musiques);
			oos.writeObject(musiquesajoute_possede);
			//envoi de la playlist avec les id mis � jour
			oos.writeObject(user_playlist);
			List<List<Integer>> possede = get_possede();
			oos.writeObject(creer_liste_suggestions(possede.get(0),possede.get(1)));
			oos.close();
			os.close();
			
			//System.out.println((String)ois.readObject());
			
		
		
		is.close();
		ois.close();
		
		socket.close();
		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public List<Musique> get_musiques(){
		List<Musique> musiquesbdd = new ArrayList<Musique>();
    	InputStream is = null;
		String result = "";
		String line;
 	   
   		// Envoie de la commande http
    	try
    	{
		HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost("http://musicshared.netne.net/get_musiques.php");
	        HttpResponse response = httpclient.execute(httppost); 
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
	        System.out.println("pass 1 - connection success ");
    	}
        catch(Exception e)
    	{
        	 System.out.println("Fail 1 - " + e.toString());
    	}    

   		// Conversion de la requ�te en string
        try
        {
            BufferedReader reader = new BufferedReader
			(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
	    {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            System.out.println("pass 2 - connection success ");
        }
        catch(Exception e)
        {
        	 System.out.println("Fail 2 - " + e.toString());
        } 
    	try
    	{	JSONArray jArray  = new JSONArray(result);
    		
    		for(int i=0;i<jArray.length();i++){
    			JSONObject json_data = jArray.getJSONObject(i);
    			
    			musiquesbdd.add(new Musique(Integer.parseInt(json_data.getString("id_musique")),json_data.getString("titre"),
    					json_data.getString("artiste"),json_data.getString("genre")));
    			System.out.println("Pass 3 - " +  musiquesbdd.get(i).getTitre() + " trouv�");
			 }
    		
    	}
        catch(Exception e)
    	{  
        	System.out.println("Fail 3 - connexion - " +  e.toString());
    	}return musiquesbdd;
    }
	
	public List<List<Integer>> get_possede(){
		InputStream is = null;
		List<Integer> ids_utilisateurs = new ArrayList<Integer>();
		List<Integer> ids_musiques = new ArrayList<Integer>();
	
		List<List<Integer>> resultat = new ArrayList<List<Integer>>();
		resultat.add(ids_utilisateurs);
		resultat.add(ids_musiques);
		String result = "";
		String line;
 	   
   		// Envoie de la commande http
    	try
    	{
		HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost("http://musicshared.netne.net/get_possede.php");
	        HttpResponse response = httpclient.execute(httppost); 
	        HttpEntity entity = response.getEntity();
	        is = entity.getContent();
	        System.out.println("pass 1 - connection success ");
    	}
        catch(Exception e)
    	{
        	 System.out.println("Fail 1 - " + e.toString());
    	}    

   		// Conversion de la requ�te en string
        try
        {
            BufferedReader reader = new BufferedReader
			(new InputStreamReader(is,"iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            while ((line = reader.readLine()) != null)
	    {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
            System.out.println("pass 2 - connection success ");
        }
        catch(Exception e)
        {
        	 System.out.println("Fail 2 - " + e.toString());
        } 
    	try
    	{	JSONArray jArray  = new JSONArray(result);
    		
    		for(int i=0;i<jArray.length();i++){
    			JSONObject json_data = jArray.getJSONObject(i);
    			
    			ids_utilisateurs.add(Integer.parseInt(json_data.getString("id_utilisateur")));
    			ids_musiques.add(Integer.parseInt(json_data.getString("id_musique")));
			 }
    		
    	}
        catch(Exception e)
    	{  
        	System.out.println("Fail 3 - connexion - " +  e.toString());
    	}
		return resultat;
		
	}
	
	public List<Integer> creer_liste_suggestions(List<Integer> ids_utilisateurs,List<Integer> ids_musiques){
		
		Set<Integer> set = new HashSet<Integer>();
		set.addAll(ids_utilisateurs);
		List<Integer> distinct_ids_utilisateurs = new ArrayList<Integer>(set);
		print_list(distinct_ids_utilisateurs);
		
		Set<Integer> set2 = new HashSet<Integer>();
		set2.addAll(ids_musiques);
		List<Integer> distinct_ids_musiques = new ArrayList<Integer>(set2);
		print_list(distinct_ids_musiques);
		
		int nb_musiques = distinct_ids_musiques.size();
		int nb_utilisateurs = distinct_ids_utilisateurs.size();
		int[][] possede = new int[nb_utilisateurs][nb_musiques];
		for (int i =0; i<nb_utilisateurs;i++){
			for(int j = 0; j<nb_musiques;j++)
			possede[i][j]=0;
		}
		for (int i =0; i<ids_utilisateurs.size();i++){
			//possede[ids_utilisateurs.get(i)][ids_musiques.get(i)]=1;
			possede[recherche(ids_utilisateurs.get(i),distinct_ids_utilisateurs)]
					[recherche(ids_musiques.get(i),distinct_ids_musiques)]=1;
		}
		
		double[][] relations = new double[nb_musiques][nb_musiques];
		for (int i =0; i<nb_musiques;i++){
			for(int j = 0; i<nb_musiques;i++)
					relations[i][j]=0;
		}
		
		for (int i =0; i<nb_musiques;i++){
			//System.out.print("i:"+i + " - ");
			for(int j = 0; j<nb_musiques;j++){
				//d�j� calcul�
				if(relations[j][i]!=0)
					relations[i][j]=relations[j][i];
				else if(i!=j){
					double cos_num = 0;
					double cos_den1 = 0;
					double cos_den2 = 0;
					for (int k = 0; k<nb_utilisateurs;k++){
						cos_num += possede[k][i] * possede[k][j];
						cos_den1 += possede[k][i];
						cos_den2 += possede[k][j];
					}
					double cos_den = Math.sqrt(cos_den1)*Math.sqrt(cos_den2);
					relations[i][j] = cos_num/cos_den;
				}
			System.out.print(relations[i][j] + "     ");
			}
		System.out.println();
		}

		int id_position = recherche(id,distinct_ids_utilisateurs);
		List<Integer> suggestions = new ArrayList<Integer>();
		for (int i = 0;i<nb_musiques;i++){
			if(possede[id_position][i]==1){
				for (int j = 0;j<nb_musiques;j++){
					if (relations[i][j]>0.6&&possede[id_position][j]==0)
						suggestions.add(distinct_ids_musiques.get(i));
				}
			}
		}
		
		
		Set<Integer> set3 = new HashSet<Integer>();
		set3.addAll(suggestions);
		List<Integer> distinct_suggestions = new ArrayList<Integer>(set3);
		print_list(distinct_suggestions);
		
		return distinct_suggestions;
		
	}
	
	
	public static void print_list(List<Integer> list){
		for (int i = 0; i<list.size();i++)
			System.out.print(list.get(i) + " ");
		System.out.println();
	}
	
	//recherche si un element appartient � une liste et si oui, renvoie sa position
	public int recherche(int n, List<Integer> list){
		for(int i=0; i<list.size();i++){
			if(list.get(i)==n)
				return i;
		}
		return -1;
	}
	
	public int add_musiques(List<Musique> m){
	 	   InputStream is = null;
	  		String result="";
	  		int musiquesajoute=0;
	  		
	  		String json_string ="{\"musiques\":[";
	  		
	  		try {
	  			for(int i = 0;i<m.size();i++){
	  			JSONObject obj_new = new JSONObject();
				obj_new.put("titre", m.get(i).getTitre());
				obj_new.put("artiste", m.get(i).getArtiste());
				obj_new.put("genre", m.get(i).getGenre());
				json_string = json_string + obj_new.toString() + ",";
	  			}
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	  	//Close JSON string
	  		json_string = json_string.substring(0, json_string.length()-1);
	  		json_string += "]}";
	  		System.out.println("json - add_musiques - " +json_string);

	  		// Envoie de la commande http
	  		try{
	  			HttpClient httpclient = new DefaultHttpClient();
	  			HttpPost httppost = new HttpPost("http://musicshared.netne.net/ajout_musique.php");
	  			httppost.setEntity(new ByteArrayEntity(json_string.getBytes("UTF8")));
	  			httppost.setHeader("json", json_string);
	  			HttpResponse response = httpclient.execute(httppost);
	  			HttpEntity entity = response.getEntity();
	  			is = entity.getContent();
	  			System.out.println("pass 1 - insert - connection success ");
	  		
	  		}catch(Exception e){
	  			System.out.println("Fail 1 - " +e.toString());
	  		}
	  		try{
	  			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	  			StringBuilder sb = new StringBuilder();
	  			String line = null;
	  			while ((line = reader.readLine()) != null) {
	  				sb.append(line + "\n");
	  			}
	  			is.close();
	  			result=sb.toString();
	  			System.out.println("pass 2 - connection success ");
	  		}catch(Exception e){
	  			System.out.println("Fail 2 - " + e.toString());
	  		}
	  		
	  		
	  		try
	    	{
	        	JSONObject json_data = new JSONObject(result);
	        	musiquesajoute=(json_data.getInt("code"));
	        
	    	}
	        catch(Exception e)
	    	{   
	        	System.out.println("Fail 3 - connexion - " + e.toString());
	    	}
	    	return musiquesajoute;
	    }
	    
	    public int add_possede(List<Musique> m, int id){
	  	   InputStream is = null;
	   		String result="";
	   		int musiquesajoute=0;
	   		
	   		String json_string ="{\"possede\":[";
	   		
	   		try {
	   			for(int i = 0;i<m.size();i++){
	   			JSONObject obj_new = new JSONObject();
	   		
	 			obj_new.put("id_utilisateur", id);
	 			obj_new.put("titre", m.get(i).getTitre());
	 			obj_new.put("artiste", m.get(i).getArtiste());
	 			json_string = json_string + obj_new.toString() + ",";
	   			}
	 		} catch (JSONException e1) {
	 			// TODO Auto-generated catch block
	 			e1.printStackTrace();
	 		}
	   	//Close JSON string
	   		json_string = json_string.substring(0, json_string.length()-1);
	   		json_string += "]}";
	   		System.out.println("json : "+json_string);
	   		// Envoie de la commande http
	   		try{
	   			HttpClient httpclient = new DefaultHttpClient();
	   			HttpPost httppost = new HttpPost("http://musicshared.netne.net/ajout_possede.php");
	   			httppost.setEntity(new ByteArrayEntity(json_string.getBytes("UTF8")));
	   			httppost.setHeader("json", json_string);
	   			HttpResponse response = httpclient.execute(httppost);
	   			HttpEntity entity = response.getEntity();
	   			is = entity.getContent();
	   			 System.out.println("pass 1 - possede - connection success ");
	 
	   		}catch(Exception e){
	   			System.out.println("Fail 1 - "+e.toString());
	   		}
	   		try{
	   			BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	   			StringBuilder sb = new StringBuilder();
	   			String line = null;
	   			while ((line = reader.readLine()) != null) {
	   				sb.append(line + "\n");
	   			}
	   			is.close();
	   			result=sb.toString();
	   			System.out.println("pass 2 - connection success ");
	   		}catch(Exception e){
	   			System.out.println("Fail 2 - " + e.toString());
	   		}
	   		
	   		
	   		try
	     	{
	         	JSONObject json_data = new JSONObject(result);
	         	musiquesajoute=(json_data.getInt("code"));
	         
	     	}
	         catch(Exception e)
	     	{   
	        	 System.out.println("Fail 3 - possede : " +  e.toString());
	     	}
	     	return musiquesajoute;
	     }
public static void main(String args[]) {

try {
	System.out.println("Lancement du serveur");

	//int[] c = new int[3];
	ServerSocket ss = new ServerSocket(port);
	
	
	while(true){
	Socket s = ss.accept();
	SimpleServer t = new SimpleServer(s);
	t.start();
	}
	
	
}catch(Exception e){System.out.println(e);}
}
}