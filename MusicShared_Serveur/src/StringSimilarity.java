

public class StringSimilarity {

    /**
     * @param args the command line arguments
     */
    
     public static double similarity(String s1, String s2) {
         int s1Length = s1.length();
         int s2Length = s2.length();
         int longerLength = s1Length;
        if (s1Length < s2Length) {
            longerLength = s2Length;
        }
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }

        return (longerLength - DistanceDeLevenshtein(s1, s2)) / (double) longerLength;
 
     }
   
    
    public static int DistanceDeLevenshtein(String s1, String s2){
        int longueurChaine1 = s1.length();
        int longueurChaine2 = s2.length();
        
        int d[][] = new int[longueurChaine1+1][longueurChaine2+1];
        
        for (int i = 0;i<=longueurChaine1;i++){
            d[i][0]=i;
        }
         for (int j = 0;j<=longueurChaine2;j++){
            d[0][j]=j;
        }
        int cout; 
        for(int i = 1;i<=longueurChaine1;i++){
            for (int j = 1;j<=longueurChaine2;j++){
                if (s1.charAt(i-1)==s2.charAt(j-1))
                    cout = 0;
                else cout = 1;
                d[i][j]=Math.min(d[i-1][j]+1,Math.min(d[i][j-1]+1,d[i-1][j-1]+cout));
            }
        }
        return d[longueurChaine1][longueurChaine2];
    }
    
 
  
}
